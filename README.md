# IPLTD

![logo](markdown/IPLTD.jpg)

This is one of my old projects that I designed for telecommunication department of Shiraz university about one decade ago using silverlight technology for recording and tracking phone lines with different access levels. This system is still active on the university intranet.

## Software features

- Ability to add, delete and edit information of phone lines.
- Advanced search using any combination of fields. 
- Ability to add, remove and edit users along with granting different access by management.
- Ability to backup and restore the database by management.
- User-friendly interface.
- Use 2-way communication technique (WCF Duplex) to record and display database changes simultaneously among all users.
- Internal mail box to send and receive messages between users.
- Password encryption with SHA256 algorithm for more security and avoid sniffing attack.
- Ability to use the software under the browser and out of the browser with the possibility of updating (silverlight OOB feature).

![logo](markdown/screen2.jpg)


## Requirement:
- [x] SQL Server
- [x] Silverlight


#### *Please note that i am not able to share source code of complete version due to copyright, But i shared a preview version of that for your information.