﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using IPLTD.ServiceReferenceMembers;
using IPLTD.ServiceReferenceDatabaseManagment;
using IPLTD.ServiceReferenceMemberEditData;

//using System.Windows.Forms;
//using Microsoft.SqlServer.Management.Smo;

using IPLTD.Class.Security;

using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;


namespace IPLTD.Windows
{
    
    public partial class Moderator_Source_ : UserControl
    {
        private ServiceMemberEditDataClient ClientEditMember = new ServiceMemberEditDataClient();
        private ServiceMembersClient ClientMember = new ServiceMembersClient();
        private ServiceDatabaseManagmentClient ClientDatabaseManager = new ServiceDatabaseManagmentClient();
        public Moderator_Source_()
        {
            
            
            InitializeComponent();
            Loading_BackupGo.Visibility = Visibility.Collapsed;
            Loading_RestoreGo.Visibility = Visibility.Collapsed;
            Loading_UploadGo.Visibility = Visibility.Collapsed;
            ProgressBar1.Visibility = Visibility.Collapsed;


            ParentDatabase.Visibility = Visibility.Collapsed;
            ParentMember.Visibility = Visibility.Visible;
            StoryboardMasterTabMemberClick.Begin();

            ClientMember.GetAllMemberCompleted += new EventHandler<GetAllMemberCompletedEventArgs>(ClientMember_GetAllMemberCompleted);
            ClientMember.GetAllMemberAsync();

            ClientDatabaseManager.BackUpCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(ClientDatabaseManager_BackUpCompleted);
            ClientDatabaseManager.RestoreCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(ClientDatabaseManager_RestoreCompleted);

        }

       
        public MailBox_Source_ WindowMailboxSource { get; set; }
        void ClientMember_GetAllMemberCompleted(object sender, GetAllMemberCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                foreach (var item in e.Result)
                {
                    WindowMailboxSource.ComboBoxMemberList.Items.Add(item._UserName);
                }
                DataGrid1.ItemsSource = e.Result;
                DataForm1.ItemsSource = e.Result;
            }
            else
                MessageBox.Show(Convert.ToString(e.Error));
        }

        void ClientDatabaseManager_RestoreCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {

            Loading_RestoreGo.Visibility = Visibility.Collapsed;

            if (e.Error == null)
            {
                Message.InfoMessage("بانک اطلاعات با موفقیت بازیابی شد.");
                Goto_lvl1_Restore();
            }
            else
            {
                Message.ErrorMessage("بازیابی بانک اطلاعات با مشکل مواجه شد!");
            }
        }

        void ClientDatabaseManager_BackUpCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {

            Loading_BackupGo.Visibility = Visibility.Collapsed;
            if (e.Error == null)
            {
                Message.InfoMessage("عملیات پشتیبان گیری از بانک اطلاعات با موفقیت انجام شد.");
                Goto_lvl2_Backup();
            }
            else
            {

                MessageBox.Show(Convert.ToString(e.Error.Message) + "\n" + "عملیات پشتیبان گیری با مشکل مواجه شد!");
           //     Message.ErrorMessage(Convert.ToString(e.Error)+"\n" + "عملیات پشتیبان گیری با مشکل مواجه شد!");
            }
        }
       
        
        






        private void DataForm1_CurrentItemChanged(object sender, EventArgs e)
        {
            try
            {
                PasswordMember.Password = "";
                DataGrid1.SelectedItem = DataForm1.CurrentItem;
                DataGrid1.ScrollIntoView(DataForm1.CurrentItem, DataGridColumn1);
            }
            catch { }
        }

        //============================================================================================================================================        
        //DataGrid============================================================================================================================================        
        private void DataGrid1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //var count = DataGrid1.ItemsSource.OfType<object>().Count();

            DataForm1.CurrentItem = DataGrid1.SelectedItem;
          

          //  TextBoxCount.Content = Convert.ToString(DataGrid1.SelectedIndex + 1) + " Of " + Convert.ToString(count);
        }

        //============================================================================================================================================        
        //Resize============================================================================================================================================        
        private void ResizeCut_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                DataForm1.SetValue(Canvas.TopProperty, DataGrid1.ActualHeight);
                DataForm1.Height = MyParent.ActualHeight - DataGrid1.ActualHeight;
            }
            catch { }
        }

        private void LayoutRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                DataForm1.Width = MyParent.ActualWidth;
                DataGrid1.Width = MyParent.ActualWidth;
                if (MyParent.ActualHeight >= DataGrid1.ActualHeight)
                {
                    DataForm1.Height = MyParent.ActualHeight - DataGrid1.ActualHeight;
                }
                else
                {
                    DataForm1.Height = 0;

                }
                DataForm1.SetValue(Canvas.TopProperty, DataGrid1.ActualHeight);
            }
            catch { }
        }
        bool AddNew = false;
        private Stream data;
  //      private Stream data;
        
        private void DataForm1_AddingNewItem(object sender, DataFormAddingNewItemEventArgs e)
        {
            AddNew = true;
        }
        private void DataForm1_EditEnding(object sender, DataFormEditEndingEventArgs e)
        {
            SaltedHash Salt = new SaltedHash();
          
            DataGridEnabled.Visibility = Visibility.Collapsed;



            if (e.EditAction == DataFormEditAction.Commit)
            {

                Members DataEditMember = DataForm1.CurrentItem as Members;

                DataEditMember._Password=Salt.hash(PasswordMember.Password);
                if (AddNew == true)
                {
                    ClientEditMember.InsertAsync(DataEditMember._Name, DataEditMember._Family, DataEditMember._UserName, DataEditMember._Password, DataEditMember._AccessEdit_State1, DataEditMember._AccessEdit_State2, DataEditMember._AccessEdit_State3, DataEditMember._AccessEdit_State4, DataEditMember._AccessEdit_State5, DataEditMember._AccessEdit_State6, DataEditMember._Manager,DataEditMember.___State,DataEditMember._AccessEdit_State7);
                    AddNew = false;
                }
                else
                {
                    ClientEditMember.EditAsync(DataEditMember._Name, DataEditMember._Family, DataEditMember._UserName, DataEditMember._Password, DataEditMember._AccessEdit_State1, DataEditMember._AccessEdit_State2, DataEditMember._AccessEdit_State3, DataEditMember._AccessEdit_State4, DataEditMember._AccessEdit_State5, DataEditMember._AccessEdit_State6, DataEditMember._Manager, DataEditMember.___State, DataEditMember._ID, DataEditMember._AccessEdit_State7);
                }
            }
            else
            {
                AddNew = false;
            }
        }

        private void DataForm1_BeginningEdit(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DataGridEnabled.Visibility = Visibility.Visible;
        }



        
        
            private void DataForm1_ValidatingItem(object sender, System.ComponentModel.CancelEventArgs e)
        {
         //   PasswordMember.BindingValidationError += new EventHandler<ValidationErrorEventArgs>(PasswordMember_BindingValidationError);
        //    s.MemberName = "DataForm1";
           
   //         s.MemberName = "PasswordMember";
       //     s.MemberName = "_UserName";
         //   if (PasswordMember.Password != "122222")
          
        //        throw new Exception("xdsfsdfsdfsdf");
         //           DataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Name must be at least 5 characters long!";
                   // DataForm1.BindingValidationError+=new EventHandler<ValidationErrorEventArgs>(DataForm1_BindingValidationError);
                   
           //     else
             //       DataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";

            
        }
            void PasswordMember_BindingValidationError(object sender, ValidationErrorEventArgs e)
            {
                
            }

            private void DataForm1_DeletingItem(object sender, System.ComponentModel.CancelEventArgs e)
            {
                Members DataEditMember = DataForm1.CurrentItem as Members;
                ClientEditMember.DeleteAsync(DataEditMember._ID);
            }

            


            void UploadProgress(long Max,long Progress )
            {

                ProgressBar1.Maximum = Max;
                Progress = Progress + Progress;
                ProgressBar1.Value = Progress;
            }
            private void ButtonTabBackup_Click(object sender, RoutedEventArgs e)
            {
                ParentRestore.Visibility = Visibility.Collapsed;
                ParentBackup.Visibility = Visibility.Visible;
                ButtonTabBackup.Opacity = 1;
                ButtonTabRestore.Opacity = 0.3;
            }

            private void ButtonTabRestore_Click(object sender, RoutedEventArgs e)
            {
                ParentRestore.Visibility = Visibility.Visible;
                ParentBackup.Visibility = Visibility.Collapsed;
                ButtonTabBackup.Opacity = 0.3;
                ButtonTabRestore.Opacity = 1;
            }

            private void ButtonMasterTabMember_Click(object sender, RoutedEventArgs e)
            {
               
                ParentDatabase.Visibility = Visibility.Collapsed;
                ParentMember.Visibility = Visibility.Visible;
                StoryboardMasterTabMemberClick.Begin();
            }

            private void ButtonMasterTabDataModerator_Click(object sender, RoutedEventArgs e)
            {
                ParentDatabase.Visibility = Visibility.Visible;
                ParentMember.Visibility = Visibility.Collapsed;
                StoryboardMasterTabDatabaseClick.Begin();
            }

            private void ButtonRestoreGo_Click(object sender, RoutedEventArgs e)
            {
                ClientDatabaseManager.RestoreAsync();
                Loading_RestoreGo.Visibility = Visibility.Visible;
            }

            private void ButtonBackupGo_Click(object sender, RoutedEventArgs e)
            {
                ClientDatabaseManager.BackUpAsync();
                Loading_BackupGo.Visibility = Visibility.Visible;
            }

            private void ButtonUpload_Click(object sender, RoutedEventArgs e)
            {
                ProgressBar1.ValueChanged += new RoutedPropertyChangedEventHandler<double>(ProgressBar1_ValueChanged);
                Loading_UploadGo.Visibility = Visibility.Visible;
                       OpenFileDialog dlg = new OpenFileDialog();
                      dlg.Multiselect = false;
                      dlg.Filter = "BackupFile|*.bak";
                      if ((bool)dlg.ShowDialog())
                      {
                          UploadFile(dlg.File.Name, dlg.File.OpenRead());
                          ProgressBar1.Visibility = Visibility.Visible;
                      }
                      else
                      {
                          Message.ErrorMessage("شما فایلی را انتخاب نکرده اید!");
                          Loading_UploadGo.Visibility = Visibility.Collapsed;
                          ProgressBar1.Visibility = Visibility.Collapsed;
                      }
            }

            void ProgressBar1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
            {
                if (ProgressBar1.Maximum <= e.NewValue)
                {
                    Goto_lvl2_Restore();
                    ProgressBar1.Value = 0;
                    ProgressBar1.Visibility = Visibility.Collapsed;
                    Loading_UploadGo.Visibility = Visibility.Collapsed;
                    
       
                }
            }
            private void UploadFile(string FileName, Stream fileStream)
            {
                data = fileStream;
                int test;
                try
                {
                    test = Convert.ToInt32(fileStream.Length);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(Convert.ToString(ex));
                }

                UriBuilder ub = new UriBuilder("http://172.16.0.10:80/receiver.ashx");
                ub.Query = string.Format("filename={0}", FileName);

                WebClient c = new WebClient();
                c.OpenWriteCompleted += new OpenWriteCompletedEventHandler(c_OpenWriteCompleted);
                c.OpenWriteAsync(ub.Uri);
            }
        
        
        
        
        
        
            SaveFileDialog _asyncSaveDialog = new SaveFileDialog();
            private void ButtonDownload_Click(object sender, RoutedEventArgs e)
            {
                System.Windows.Browser.HtmlPage.Window.Navigate(new Uri("http://172.16.0.10:80/Backupdb/HWS.bak"));
                Goto_lvl1_Backup();
            }
            void c_OpenWriteCompleted(object sender, OpenWriteCompletedEventArgs e)
            {
                PushData(data, e.Result);
                e.Result.Close();
                data.Close();
            }


            private void PushData(Stream input, Stream output)
            {
                long ProgMax = input.Length;
                byte[] Buffer = new byte[4096];
                int BytesRead;
                while ((BytesRead = input.Read(Buffer, 0, Buffer.Length)) != 0)
                {
                    output.Write(Buffer, 0, BytesRead);
                    UploadProgress(ProgMax, output.Length);
                }

            }



            void Goto_lvl2_Backup()
            {
                Accept_Backup_lvl1.Visibility = Visibility.Visible;
                lvl2Column_Backup.Opacity = 1;
                lvl2Column2_Backup.Opacity = 1;
                ButtonDownload.IsEnabled = true;
            }

            void Goto_lvl1_Backup()
            {
                Accept_Backup_lvl1.Visibility = Visibility.Collapsed;
                lvl2Column_Backup.Opacity = 0.40;
                lvl2Column2_Backup.Opacity = 0.40;
                ButtonDownload.IsEnabled = false;
            }

            void Goto_lvl2_Restore()
            {
                Accept_Upload_lvl1.Visibility = Visibility.Visible;
                lvl2Column2_Restore.Opacity = 1;
                lvl2Column_Restore.Opacity = 1;
                ButtonRestoreGo.IsEnabled = true;
            }

            void Goto_lvl1_Restore()
            {
                Accept_Upload_lvl1.Visibility = Visibility.Collapsed;
                lvl2Column2_Restore.Opacity = 0.40;
                lvl2Column_Restore.Opacity = 0.40;
                ButtonRestoreGo.IsEnabled = false;
            }

            private void ButtonEdit_Click(object sender, RoutedEventArgs e)
            {
                DataForm1.BeginEdit();
            }

            private void ButtonAdd_Click(object sender, RoutedEventArgs e)
            {
                DataForm1.AddNewItem();
            }

            private void ButtonDelete_Click(object sender, RoutedEventArgs e)
            {
                DataForm1.DeleteItem();
            }
         
        

    }
}










        

