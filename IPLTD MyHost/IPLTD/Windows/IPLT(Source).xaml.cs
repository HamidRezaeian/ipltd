﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using IPLTD.Windows;
using IPLTD.Class;
using System.ServiceModel;
using System.ServiceModel.Channels;
using IPLTD.ServiceReferenceIPL;
using IPLTD.ServiceReferenceIPLEditData;
using IPLTD.ServiceReferenceIPLDuplex;

namespace IPLTD.Windows
{

    public partial class IPLT_Source_ : UserControl
    {
        
     //   private ServiceDuplex_IPLClient DuplexIPL = new ServiceDuplex_IPLClient("myNetTcpEndpoint");

       private ServiceDuplex_IPLClient DuplexIPL = new ServiceDuplex_IPLClient(new PollingDuplexHttpBinding(PollingDuplexMode.MultipleMessagesPerPoll), new EndpointAddress("../Services/ServiceDuplex_IPL.svc")) ;
        
        private ServiceIPLTClient ClientIPL = new ServiceIPLTClient();
        private ServiceIPLEditDataClient ClientIPLEdit = new ServiceIPLEditDataClient();
        public IPLT_Source_()
        {
            InitializeComponent();

            DuplexIPL.OnMessageReceived += new EventHandler<OnMessageReceivedEventArgs>(DuplexIPL_OnMessageReceived);
            
            ClientIPL.GetAllInformationCompleted += new EventHandler<GetAllInformationCompletedEventArgs>(ClientIPL_GetAllInformationCompleted);
            ClientIPL.GetAllInformationAsync();
            ClientIPLEdit.InsertCompleted += new EventHandler<InsertCompletedEventArgs>(ClientIPLEdit_InsertCompleted);
            ClientIPLEdit.EditCompleted += new EventHandler<EditCompletedEventArgs>(ClientIPLEdit_EditCompleted);

            this.Unloaded += new RoutedEventHandler(IPLT_Source__Unloaded);
            this.Loaded += new RoutedEventHandler(IPLT_Source__Loaded);
        }

        
       

        void IPLT_Source__Loaded(object sender, RoutedEventArgs e)
        {
            DuplexIPL.AddClientAsync();
            
        }

        void IPLT_Source__Unloaded(object sender, RoutedEventArgs e)
        {

            DuplexIPL.RemoveClientAsync();
        }

//-------------------------------------------------------------------------------------------------------------------------------------------------
//MyEvent-------------------------------------------------------------------------------------------------------------------------------------------------
        ObservableCollection<IPL> Result = new ObservableCollection<IPL>();
        bool PutResult = false;
        public void DataGrid_Update(
            string Action,int Index,string Telephone, int State, string Post_Master,
            string Connection, string Post1, string Post2, string Post3,
            string Post4, string Post5, string User_, string Kafoo,string Position,Int64 ID)
        {

            IPL X = new IPL();
            X.__ID = ID;
            if (Connection != null)
                X.__Connection = Connection;
            else
                X.__Connection = "";

            if (Kafoo != null)
                X.__Kafoo = Kafoo;
            else
                X.__Kafoo = "";

            if (Telephone != null)
                X.__Telephone = Telephone;
            else
                X.__Telephone = "";

            if (Post_Master != null)
                X.__Post_Master = Post_Master;
            else
                X.__Post_Master = "";

            if (Post1 != null)
                X.__Post1 = Post1;
            else
                X.__Post1 = "";

            if (Post2 != null)
                X.__Post2 = Post2;
            else
                X.__Post2 = "";

            if (Post3 != null)
                X.__Post3 = Post3;
            else
                X.__Post3 = "";

            if (Post4 != null)
                X.__Post4 = Post4;
            else
                X.__Post4 = "";

            if (Post5 != null)
                X.__Post5 = Post5;
            else
                X.__Post5 = "";

            X.__State = State;

            if (User_ != null)
                X.__User_ = User_;
            else
                X.__User_ = "";

            if (Position != null)
                X.__Position = Position;
            else
                X.__Position = "";

            if (Action == "Insert")
            {
                if (PutResult == true)
                {
                    Result.Add(X);
                }
                else
                {
                        Result.Add(X);
                        Search();
                }
            }
            if (Action == "Edit")
            {




                foreach (var item in Result)
                {
                    
                    if (item.__ID == ID)
                    {
                        Result.Remove(item);

                        break;
                    }
                }
           //     


                Result.Insert(Index, X);
                
                
        //        Search();
            }
            IPL X1 = new IPL();
            X1.__Connection = Connection;
            X1.__ID = ID;
            X1.__Kafoo = Kafoo;
            X1.__Position = Position;
            X1.__Post_Master = Post_Master;
            X1.__Post1 = Post1;
            X1.__Post2 = Post2;
            X1.__Post3 = Post3;
            X1.__Post4 = Post4;
            X1.__Post5 = Post5;
            X1.__State = State;
            X1.__Telephone = Telephone;
            X1.__User_ = User_;
          
            if (Action == "Delete")
            {
                DataGrid1.ItemsSource = Result.Where(a => a.__Kafoo.Contains("")
                & a.__Telephone.Contains("")
                & a.__User_.Contains("")
                & a.__Position.Contains("")
                & a.__Connection.Contains("")
                & a.__Post_Master.Contains("")
                & Convert.ToString(a.__State).Contains("")
                & a.__Post1.Contains("")
                & a.__Post2.Contains("")
                & a.__Post3.Contains("")
                & a.__Post4.Contains("")
                & a.__Post5.Contains("")).ToList();
                DataForm1.ItemsSource = DataGrid1.ItemsSource; 
                foreach (var item in Result)
                {
                    if (item.__ID == ID)
                    {
                        Result.Remove(item);
                        
                        break;
                    }

                    Search();
                }
            }
        }
        public ObservableCollection<IPL> Test(System.Collections.IEnumerable original)
        {
            return new ObservableCollection<IPL>(original.Cast<IPL>());
        }
     


        string SearchValueTelephone="";
        string SearchValueKafoo="";
        string SearchValueUser = "";
        string SearchValuePosition = "";
        string SearchValueConnection = "";
        string SearchValueMasterPost = "";
        string SearchValueState = "";
        string SearchValuePost1 = "";
        string SearchValuePost2 = "";
        string SearchValuePost3 = "";
        string SearchValuePost4 = "";
        string SearchValuePost5 = "";
     
        void Search()
        {
                DataGrid1.ItemsSource = Result.Where(a => a.__Kafoo.Contains(SearchValueKafoo)
                & a.__Telephone.Contains(SearchValueTelephone)
                & a.__User_.Contains(SearchValueUser)
                & a.__Position.Contains(SearchValuePosition)
                & a.__Connection.Contains(SearchValueConnection)
                & a.__Post_Master.Contains(SearchValueMasterPost)
                & Convert.ToString(a.__State).Contains(SearchValueState)
                & a.__Post1.Contains(SearchValuePost1)
                & a.__Post2.Contains(SearchValuePost2)
                & a.__Post3.Contains(SearchValuePost3)
                & a.__Post4.Contains(SearchValuePost4)
                & a.__Post5.Contains(SearchValuePost5)).ToList();
                DataForm1.ItemsSource = DataGrid1.ItemsSource;
        }
      
//-------------------------------------------------------------------------------------------------------------------------------------------------
//ServicesEvent============================================================================================================================================        
        long HWSFindID;
        void ClientIPLEdit_InsertCompleted(object sender, InsertCompletedEventArgs e)
        {
                DataGrid_Update("Insert", AccessIndex, AccessTelephone, AccessState, AccessPost_Master, AccessConnection, AccessPost1, AccessPost2, AccessPost3, AccessPost4, AccessPost5, AccessUser_, AccessKafoo, AccessPosition, HWSFindID);
            if (e.Error == null)
            {
                HWSFindID= e.Result;
                DuplexSender = true;
                DuplexIPL.SendMessageAsync("Insert", AccessIndex, AccessTelephone,AccessState,AccessPost_Master,AccessConnection,AccessPost1,AccessPost2,AccessPost3,AccessPost4,AccessPost5,AccessUser_,AccessKafoo,AccessPosition,HWSFindID);
            }
            else
            {
                MessageBox.Show(Convert.ToString(e.Error));
            
            }
        }
        bool DuplexSender = false;
        void DuplexIPL_OnMessageReceived(object sender, OnMessageReceivedEventArgs e)
        {
            if (DuplexSender == false)
            {
                if (e.Error == null)
                    DataGrid_Update(e.Action, e.Index, e.Telephone, e.State, e.Post_Master, e.Connection, e.Post1, e.Post2, e.Post3, e.Post4, e.Post5, e.User_, e.Kafoo, e.Position, e.ID);
                else
                    MessageBox.Show(Convert.ToString(e.Error));
            }
            else
            {
                DuplexSender = false;
            }
        }

        void ClientIPLEdit_EditCompleted(object sender, EditCompletedEventArgs e)
        {
       //     if (DataGrid1.ItemsSource != null)
        //    {
         //       Result = Test(DataGrid1.ItemsSource);
         //   }
        }
        void ClientIPL_GetAllInformationCompleted(object sender, GetAllInformationCompletedEventArgs e)
        {

            ButtonUpdate.IsEnabled = true;
           ObservableCollection<IPL> NullList = new ObservableCollection<IPL>();
           Result = NullList;
         //   DataForm1.ItemsSource = e.Result;
         //   DataGrid1.ItemsSource = DataForm1.ItemsSource;
            PutResult = true;
            foreach (var item in e.Result)
            {
                DataGrid_Update("Insert", 0, item._Telephone, item._State, item._Post_Master, item._Connection, item._Post1, item._Post2, item._Post3, item._Post4, item._Post5, item._User_, item._Kafoo,item._Position,item._ID);
            }
            PutResult = false;
            Search();
       //     DataForm1.ItemsSource = e.Result;
         //   DataGrid1.ItemsSource = DataForm1.ItemsSource;
            
        }
//============================================================================================================================================        
//Resize============================================================================================================================================        
        private void ResizeCut_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                DataForm1.SetValue(Canvas.TopProperty, DataGrid1.ActualHeight);
                DataForm1.Height = MyParent.ActualHeight - DataGrid1.ActualHeight;
            }
            catch { }
        }

        private void LayoutRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                DataForm1.Width = MyParent.ActualWidth;
                DataGrid1.Width = MyParent.ActualWidth;
                if (MyParent.ActualHeight >= DataGrid1.ActualHeight)
                {
                    DataForm1.Height = MyParent.ActualHeight - DataGrid1.ActualHeight;
                }
                else
                {
                    DataForm1.Height = 0;

                }
                DataForm1.SetValue(Canvas.TopProperty, DataGrid1.ActualHeight);
            }
            catch { }
        }

//============================================================================================================================================        
//DataFrom============================================================================================================================================        
        Boolean AddNew=false;
        private void DataForm1_AddingNewItem(object sender, DataFormAddingNewItemEventArgs e)
        {
            DataGridEnabled.Visibility = Visibility.Visible;
            AddNew = true;
        }

        private void DataForm1_BeginningEdit(object sender, System.ComponentModel.CancelEventArgs e)
        {
            IPL DataEditIPL = DataForm1.CurrentItem as IPL;

            if (Security_Getaccess(DataEditIPL.__State) == true)
                DataGridEnabled.Visibility = Visibility.Visible;
            else
            {
        
                ButtonAdd.IsEnabled = true;
                ButtonDelete.IsEnabled = true;
                ButtonEdit.IsEnabled = true;
                ButtonUpdate.IsEnabled = true;
                e.Cancel = true;

                Message.ErrorMessage("کاربر گرامی : شما مجوز ویرایش اطلاعات جاری را ندارید.");
   
            }
        }

        



        string AccessTelephone;int AccessState;string AccessPost_Master;string AccessConnection;
        string AccessPost1;string AccessPost2;string AccessPost3;string AccessPost4;
        string AccessPost5;string AccessUser_;Int64 AccessID;string AccessKafoo;
        int AccessIndex; string AccessPosition;











        int IntDuplicate;
        bool Duplicate;
        private void DataForm1_EditEnding(object sender, DataFormEditEndingEventArgs e)
        {
            
            IPL DataEditIPL = DataForm1.CurrentItem as IPL;
            if (e.EditAction != DataFormEditAction.Cancel)
            {
                if (Security_Getaccess(DataEditIPL.__State) == true)
                {
                    foreach (var item in Result)
                    {

                        IntDuplicate = 0;
                        Duplicate = false;
                     //   if (DataEditIPL.__Connection == null) DataEditIPL.__Connection = "";
                     //   if (DataEditIPL.__Kafoo == null) DataEditIPL.__Kafoo = "";
                     //   if (DataEditIPL.__Position == null) DataEditIPL.__Position = "";
                     //   if (DataEditIPL.__Post_Master == null) DataEditIPL.__Post_Master = "";
                     //   if (DataEditIPL.__Post1 == null) DataEditIPL.__Post1 = "";
                     //   if (DataEditIPL.__Post2 == null) DataEditIPL.__Post2 = "";
                     //   if (DataEditIPL.__Post3 == null) DataEditIPL.__Post3 = "";
                     //   if (DataEditIPL.__Post4 == null) DataEditIPL.__Post4 = "";
                     //   if (DataEditIPL.__Post5 == null) DataEditIPL.__Post5 = "";
                        if (DataEditIPL.__Telephone == null) DataEditIPL.__Telephone = "";
                     //   if (DataEditIPL.__User_ == null) DataEditIPL.__User_ = "";




                 //       if (item.__Connection.Trim() == DataEditIPL.__Connection.Trim()) IntDuplicate++;
                   //     if (item.__Kafoo.Trim() == DataEditIPL.__Kafoo.Trim()) IntDuplicate++;
                     //   if (item.__Position.Trim() == DataEditIPL.__Position.Trim()) IntDuplicate++;
                     //   if (item.__Post_Master.Trim() == DataEditIPL.__Post_Master.Trim()) IntDuplicate++;
                     //   if (item.__Post1.Trim() == DataEditIPL.__Post1.Trim()) IntDuplicate++;
                     //   if (item.__Post2.Trim() == DataEditIPL.__Post2.Trim()) IntDuplicate++;
                     //   if (item.__Post3.Trim() == DataEditIPL.__Post3.Trim()) IntDuplicate++;
                     //   if (item.__Post4.Trim() == DataEditIPL.__Post4.Trim()) IntDuplicate++;
                     //   if (item.__Post5.Trim() == DataEditIPL.__Post5.Trim()) IntDuplicate++;
                     //   if (item.__State == DataEditIPL.__State) IntDuplicate++;
                        if (item.__Telephone.Trim() == DataEditIPL.__Telephone.Trim()) IntDuplicate++;
                     //   if (item.__User_.Trim() == DataEditIPL.__User_.Trim()) IntDuplicate++;

                        if (IntDuplicate == 1) Duplicate = true;
                    }

                    DataGridEnabled.Visibility = Visibility.Collapsed;
                    if (e.EditAction == DataFormEditAction.Commit)
                    {

                        if (Duplicate == false)
                        {
                            AccessConnection = DataEditIPL.__Connection;
                            AccessKafoo = DataEditIPL.__Kafoo;
                            AccessPost_Master = DataEditIPL.__Post_Master;
                            AccessPost1 = DataEditIPL.__Post1;
                            AccessPost2 = DataEditIPL.__Post2;
                            AccessPost3 = DataEditIPL.__Post3;
                            AccessPost4 = DataEditIPL.__Post4;
                            AccessPost5 = DataEditIPL.__Post5;
                            AccessState = DataEditIPL.__State;
                            AccessTelephone = DataEditIPL.__Telephone;
                            AccessUser_ = DataEditIPL.__User_;
                            AccessID = DataEditIPL.__ID;
                            AccessIndex = DataForm1.CurrentIndex;
                            AccessPosition = DataEditIPL.__Position;


                            if (AddNew == true)
                            {
                                ClientIPLEdit.InsertAsync(DataEditIPL.__Telephone, DataEditIPL.__State, DataEditIPL.__Post_Master, DataEditIPL.__Connection, DataEditIPL.__Post1, DataEditIPL.__Post2, DataEditIPL.__Post3, DataEditIPL.__Post4, DataEditIPL.__Post5, DataEditIPL.__User_, DataEditIPL.__Kafoo, DataEditIPL.__Position);
                                //              DuplexIPL.SendMessageAsync("Insert",DataForm1.CurrentIndex, DataEditIPL._Telephone, DataEditIPL._State, DataEditIPL._Post_Master, DataEditIPL._Connection, DataEditIPL._Post1, DataEditIPL._Post2, DataEditIPL._Post3, DataEditIPL._Post4, DataEditIPL._Post5, DataEditIPL._User_, DataEditIPL._Kafoo, DataEditIPL._ID);
                                AddNew = false;
                            }
                            else
                            {
                                ClientIPLEdit.EditAsync(DataEditIPL.__Telephone, DataEditIPL.__State, DataEditIPL.__Post_Master, DataEditIPL.__Connection, DataEditIPL.__Post1, DataEditIPL.__Post2, DataEditIPL.__Post3, DataEditIPL.__Post4, DataEditIPL.__Post5, DataEditIPL.__User_, DataEditIPL.__Kafoo, DataEditIPL.__Position, DataEditIPL.__ID);
                                DuplexSender = true;
                                DataGrid_Update("Edit", DataForm1.CurrentIndex, DataEditIPL.__Telephone, DataEditIPL.__State, DataEditIPL.__Post_Master, DataEditIPL.__Connection, DataEditIPL.__Post1, DataEditIPL.__Post2, DataEditIPL.__Post3, DataEditIPL.__Post4, DataEditIPL.__Post5, DataEditIPL.__User_, DataEditIPL.__Kafoo, DataEditIPL.__Position, DataEditIPL.__ID);
                                DuplexIPL.SendMessageAsync("Edit", DataForm1.CurrentIndex, DataEditIPL.__Telephone, DataEditIPL.__State, DataEditIPL.__Post_Master, DataEditIPL.__Connection, DataEditIPL.__Post1, DataEditIPL.__Post2, DataEditIPL.__Post3, DataEditIPL.__Post4, DataEditIPL.__Post5, DataEditIPL.__User_, DataEditIPL.__Kafoo, DataEditIPL.__Position, DataEditIPL.__ID);
                            }

                        }
                        else
                        {
                            Message.ErrorMessage("کاربر گرامی : اطلاعات جاری قبلا در بانک اطلاعات ثبت شده است.");
                            AddNew = false;
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        AddNew = false;

                    }


                    ButtonAdd.IsEnabled = true;
                    ButtonDelete.IsEnabled = true;
                    ButtonEdit.IsEnabled = true;
                    ButtonUpdate.IsEnabled = true;
                }
                else
                {

                    ButtonAdd.IsEnabled = true;
                    ButtonDelete.IsEnabled = true;
                    ButtonEdit.IsEnabled = true;
                    ButtonUpdate.IsEnabled = true;
                    e.Cancel = true;

                    Message.ErrorMessage("کاربر گرامی : شما مجوز ویرایش اطلاعات جاری را ندارید.");

                }

            }
            else
            {

                DataGridEnabled.Visibility = Visibility.Collapsed;
                AddNew = false;

                ButtonAdd.IsEnabled = true;
                ButtonDelete.IsEnabled = true;
                ButtonEdit.IsEnabled = true;
                ButtonUpdate.IsEnabled = true;
            }
        }

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        public Personal WindowPersonal { get; set; }
        bool Security_Getaccess(int State)
        {
            bool access;
            access = false;
            if (State==1)
            {
                if (WindowPersonal.WindowSource.CheckboxState1.IsChecked == true)
                   access=true;
            }

            if (State==2)
            {
                if (WindowPersonal.WindowSource.CheckboxState2.IsChecked == true)
                   access=true; 
            }

            if (State==3)
            {
                if (WindowPersonal.WindowSource.CheckboxState3.IsChecked == true)
                    access=true;
            }
            if (State==4)
            {
                if (WindowPersonal.WindowSource.CheckboxState4.IsChecked == true)
                    access=true;
            }
            if (State==5)
            {
                if (WindowPersonal.WindowSource.CheckboxState5.IsChecked == true)
                    access=true;
            }
            if (State==6)
            {
                if (WindowPersonal.WindowSource.CheckboxState6.IsChecked == true)
                    access = true;
            }
            if (State == 7)
            {
                if (WindowPersonal.WindowSource.CheckboxState7.IsChecked == true)
                    access = true;
            }
            return access;

        }

        private void DataForm1_DeletingItem(object sender, System.ComponentModel.CancelEventArgs e)
        {
         
           
           
            IPL DataEditIPL = DataForm1.CurrentItem as IPL;

            if (Security_Getaccess(DataEditIPL.__State) == true)
            {
                ClientIPLEdit.DeleteAsync(DataEditIPL.__ID);
                DuplexSender = true;
                DataGrid_Update("Delete", DataForm1.CurrentIndex, DataEditIPL.__Telephone, DataEditIPL.__State, DataEditIPL.__Post_Master, DataEditIPL.__Connection, DataEditIPL.__Post1, DataEditIPL.__Post2, DataEditIPL.__Post3, DataEditIPL.__Post4, DataEditIPL.__Post5, DataEditIPL.__User_, DataEditIPL.__Kafoo, DataEditIPL.__Position, DataEditIPL.__ID);
                DuplexIPL.SendMessageAsync("Delete", DataForm1.CurrentIndex, DataEditIPL.__Telephone, DataEditIPL.__State, DataEditIPL.__Post_Master, DataEditIPL.__Connection, DataEditIPL.__Post1, DataEditIPL.__Post2, DataEditIPL.__Post3, DataEditIPL.__Post4, DataEditIPL.__Post5, DataEditIPL.__User_, DataEditIPL.__Kafoo, DataEditIPL.__Position, DataEditIPL.__ID);
            }
            else
            {
                ButtonAdd.IsEnabled = true;
                ButtonDelete.IsEnabled = true;
                ButtonEdit.IsEnabled = true;
                ButtonUpdate.IsEnabled = true;

                e.Cancel = true;
                
                Message.ErrorMessage("کاربر گرامی : شما مجوز حذف اطلاعات جاری را ندارید.");
   
            }
        }
        
        
        private void DataForm1_CurrentItemChanged(object sender, EventArgs e)
        {
            try
            {
                DataGrid1.SelectedItem = DataForm1.CurrentItem;
                DataGrid1.ScrollIntoView(DataForm1.CurrentItem, DataGridColumn1);
            }
            catch { }
        }

//============================================================================================================================================        
//DataGrid============================================================================================================================================        
        private void DataGrid1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var count = DataGrid1.ItemsSource.OfType<object>().Count();

            DataForm1.CurrentItem = DataGrid1.SelectedItem;

            
            TextBoxCount.Content = Convert.ToString(DataGrid1.SelectedIndex + 1) +" Of "+ Convert.ToString(count);   
        }
//============================================================================================================================================        
 
        bool SearchBoxShow = false;
        Thickness TH;
        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            if (SearchBoxShow == false)
            {
                ButtonSearch.IsEnabled = false;
                SearchBoxShow = true;
                DataForm1.Width = MyParent.ActualWidth - 254;
                StoryboardShowSearchBox.Begin();
            }
            else
            {
                ButtonSearch.IsEnabled = false;
                TH.Bottom = MyParent.Margin.Bottom;
                TH.Left = MyParent.Margin.Left;
                TH.Right = 0;
                TH.Top = MyParent.Margin.Top;
                MyParent.Margin = TH;
                DataForm1.Width = MyParent.ActualWidth+254 ;
                DataGrid1.Width = MyParent.ActualWidth+254 ;
                SearchBoxShow = false;
                StoryboardCloseSearchBox.Begin();
                
            }

        }

        private void StoryboardShowSearchBox_Completed(object sender, EventArgs e)
        {
            ButtonSearch.IsEnabled = true;
        }

        private void StoryboardCloseSearchBox_Completed(object sender, EventArgs e)
        {
            ButtonSearch.IsEnabled = true;
        }

        private void MyParent_MouseMove(object sender, MouseEventArgs e)
        {
            DataForm1.SetValue(Canvas.TopProperty, DataGrid1.ActualHeight);
            DataForm1.Height = MyParent.ActualHeight - DataGrid1.ActualHeight;
        }
       
        private void SearchKafoo_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueKafoo = txt.Text;
            Search();
        }
        private void SearchTel_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueTelephone = txt.Text;
            Search();
        }
        private void SearchUser_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueUser = txt.Text;
            Search();
        }

        private void SearchPosition_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValuePosition = txt.Text;
            Search();
        }

        private void SearchConnection_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueConnection = txt.Text;
            Search();
        }

        private void SearchMasterPost_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueMasterPost = txt.Text;
            Search();
        }

        private void SearchState_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueState = txt.Text;
            Search();
        }

        private void Pro_SearchBoxUser_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueUser = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
            Search();
        }

        private void Pro_SearchBoxPosition_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValuePosition = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }

        
        private void Pro_SearchBoxConnection_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueConnection = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }

        private void Pro_SearchBoxMasterPost_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueMasterPost = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }

        private void Pro_SearchBoxKafoo_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueKafoo = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }

        private void Pro_SearchBoxState_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueState = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }

        private void Pro_SearchBoxTel_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValueTelephone = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }
       
        private void Pro_ButtonSeach_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Pro_CheckBoxAuto_Checked(object sender, RoutedEventArgs e)
        {
            Pro_ButtonSeach.Visibility = Visibility.Collapsed;
        }

        private void Pro_CheckBoxAuto_Unchecked(object sender, RoutedEventArgs e)
        {
            Pro_ButtonSeach.Visibility = Visibility.Visible;
       
        }

        private void Pro_SearchBoxPost1_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValuePost1 = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }

        private void Pro_SearchBoxPost2_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValuePost2 = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }

        private void Pro_SearchBoxPost3_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValuePost3 = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }

        private void Pro_SearchBoxPost4_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValuePost4 = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }

        private void Pro_SearchBoxPost5_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            SearchValuePost5 = txt.Text;
            if (Pro_CheckBoxAuto.IsChecked == true)
                Search();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            DataForm1.AddNewItem();
            ButtonAdd.IsEnabled = false;
            ButtonEdit.IsEnabled = false;
            ButtonDelete.IsEnabled = false;
            ButtonUpdate.IsEnabled = false;
        }

        private void ButtonEdit_Click(object sender, RoutedEventArgs e)
        {
            DataForm1.BeginEdit();
            IPL DataEditIPL = DataForm1.CurrentItem as IPL;

            if (Security_Getaccess(DataEditIPL.__State) == true)
            {
                ButtonEdit.IsEnabled = false;
                ButtonAdd.IsEnabled = false;
                ButtonDelete.IsEnabled = false;
                ButtonUpdate.IsEnabled = false;
            }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            DataForm1.DeleteItem();
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            ClientIPL.GetAllInformationAsync();
            ButtonUpdate.IsEnabled = false;
        }
        public LoginPage Loginpage1 { get; set; }
        
        
        private void ComboBox_MouseMove(object sender, MouseEventArgs e)
        {

            Loginpage1.SET_Focus(Loginpage1.IPLT1);
        }

        
        

        
       

        

    }
}
