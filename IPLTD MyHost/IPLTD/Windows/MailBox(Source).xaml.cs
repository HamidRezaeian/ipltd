﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using IPLTD.ServiceReferenceMailBox;
using IPLTD.ServiceReferenceMailBox_Edit;
namespace IPLTD.Windows
{
    public partial class MailBox_Source_ : UserControl
    {
        private MailBox_EditClient ClientMailBoxEdit = new MailBox_EditClient();
        public MailboxClient ClientMailBox = new MailboxClient();
        public MailBox_Source_()
        {
            InitializeComponent();
            ClientMailBox.GetAllInformationCompleted += new EventHandler<GetAllInformationCompletedEventArgs>(ClientMailBox_GetAllInformationCompleted);
        }
        public Top Mytop { get; set; }
        int EmailCount;
        void ClientMailBox_GetAllInformationCompleted(object sender, GetAllInformationCompletedEventArgs e)
        {
            int i = new int();
            DataGrid1.ItemsSource = e.Result;
            EmailCount = e.Result.Count;
            ButtonCheckMail.Content = "("+"پیغام ها"+" ("+e.Result.Count;
            foreach (var item in e.Result)
            {
                if (item._Read == false)
                    i++;
            }
            if (i > 0)
            {
                Mytop.Label_MailCount.Text = Convert.ToString(i);
                Mytop.Parent_ButtonReciveMailIcon.Visibility = Visibility.Visible;
            }
        }





















        private void DataForm1_CurrentItemChanged(object sender, EventArgs e)
        {
        }

        //============================================================================================================================================        
        //DataGrid============================================================================================================================================        

        private void DataGrid1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CheckBoxRead.IsChecked = true;
        //    MessageBox.Show("sadas");
            DataForm2.CurrentItem = DataGrid1.SelectedItem;
            //var count = DataGrid1.ItemsSource.OfType<object>().Count();

            //            DataForm1.CurrentItem = DataGrid1.SelectedItem;


            //  TextBoxCount.Content = Convert.ToString(DataGrid1.SelectedIndex + 1) + " Of " + Convert.ToString(count);

        }

        //============================================================================================================================================        
        //Resize============================================================================================================================================        
        private void ResizeCut_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                      DataForm1.SetValue(Canvas.TopProperty, DataGrid1.ActualHeight);
                      DataForm1.Height = MyParent.ActualHeight - DataGrid1.ActualHeight;
            }
            catch { }
        }
       // bool AddNew = false;
        private void DataForm1_AddingNewItem(object sender, DataFormAddingNewItemEventArgs e)
        {
      //      AddNew = true;
        }
        private void DataForm1_EditEnding(object sender, DataFormEditEndingEventArgs e)
        {
            //    SaltedHash Salt = new SaltedHash();

            //    DataGridEnabled.Visibility = Visibility.Collapsed;



     //       if (e.EditAction == DataFormEditAction.Commit)
       //     {

                //      Members DataEditMember = DataForm1.CurrentItem as Members;

                //    DataEditMember._Password = Salt.hash(PasswordMember.Password);
         //       if (AddNew == true)
           //     {
                    //              ClientEditMember.InsertAsync(DataEditMember._Name, DataEditMember._Family, DataEditMember._UserName, DataEditMember._Password, DataEditMember._AccessEdit_State1, DataEditMember._AccessEdit_State2, DataEditMember._AccessEdit_State3, DataEditMember._AccessEdit_State4, DataEditMember._AccessEdit_State5, DataEditMember._AccessEdit_State6, DataEditMember._Manager, DataEditMember.___State);
             //       AddNew = false;
              //  }
              //  else
               // {
                    //            ClientEditMember.EditAsync(DataEditMember._Name, DataEditMember._Family, DataEditMember._UserName, DataEditMember._Password, DataEditMember._AccessEdit_State1, DataEditMember._AccessEdit_State2, DataEditMember._AccessEdit_State3, DataEditMember._AccessEdit_State4, DataEditMember._AccessEdit_State5, DataEditMember._AccessEdit_State6, DataEditMember._Manager, DataEditMember.___State, DataEditMember._ID);
             //   }
        //    }
          //  else
          //  {
            //    AddNew = false;
          //  }
        }

        private void DataForm1_BeginningEdit(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //        DataGridEnabled.Visibility = Visibility.Visible;
        }





        private void DataForm1_ValidatingItem(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //   PasswordMember.BindingValidationError += new EventHandler<ValidationErrorEventArgs>(PasswordMember_BindingValidationError);
            //    s.MemberName = "DataForm1";

            //         s.MemberName = "PasswordMember";
            //     s.MemberName = "_UserName";
            //   if (PasswordMember.Password != "122222")

            //        throw new Exception("xdsfsdfsdfsdf");
            //           DataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Name must be at least 5 characters long!";
            // DataForm1.BindingValidationError+=new EventHandler<ValidationErrorEventArgs>(DataForm1_BindingValidationError);

            //     else
            //       DataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";


        }
        void PasswordMember_BindingValidationError(object sender, ValidationErrorEventArgs e)
        {

        }

        private void DataForm1_DeletingItem(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //     Members DataEditMember = DataForm1.CurrentItem as Members;
            //     ClientEditMember.DeleteAsync(DataEditMember._ID);
        }

        private void LayoutRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                DataForm1.Width = MyParent.ActualWidth;
                DataGrid1.Width = MyParent.ActualWidth;
                if (MyParent.ActualHeight >= DataGrid1.ActualHeight)
                {
                    DataForm1.Height = MyParent.ActualHeight - DataGrid1.ActualHeight;
                }
                else
                {
                    DataForm1.Height = 0;

                }
                DataForm1.SetValue(Canvas.TopProperty, DataGrid1.ActualHeight);
            }
            catch { }
          
        }

        private void ButtonCheckMail_Click(object sender, RoutedEventArgs e)
        {
            ParentCheckMail.Opacity = 1;
            NewMailParent.Visibility = Visibility.Collapsed;
            ButtonCheckMail.Opacity = 1;
            ButtonNewMail.Opacity = 0.3;
        }

        private void ButtonNewMail_Click(object sender, RoutedEventArgs e)
        {
            ParentCheckMail.Opacity = 0;
            NewMailParent.Visibility = Visibility.Visible;
            ButtonCheckMail.Opacity = 0.3;
            ButtonNewMail.Opacity = 1;
        }
        public Personal_Source_ WindowPersonalSource { get; set; }
        private void ButtonSendMail_Click(object sender, RoutedEventArgs e)
        {
            ClientMailBoxEdit.InsertCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(ClientMailBoxEdit_InsertCompleted);
            RichTextBoxSubject.SelectAll();
            ClientMailBoxEdit.InsertAsync(RichTextBoxSubject.Selection.Text, Convert.ToString(WindowPersonalSource.LabelUsername.Content), "", "Byte " + Convert.ToString(RichTextBoxSubject.Selection.Text.Length * 8), Convert.ToString(ComboBoxMemberList.SelectedValue), false, TextBoxHeader.Text); 
        }

        void ClientMailBoxEdit_InsertCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error == null)
                Message.InfoMessage("پیغام شما با موفقیت ارسال شد.");
            else
                Message.ErrorMessage("ارسال پیغام با مشکل مواجه شد!");
        }

        private void DataForm2_CurrentItemChanged(object sender, EventArgs e)
        {
            MailboxClass DataEditMailBox = DataForm2.CurrentItem as MailboxClass;
            if (DataEditMailBox._Read == false)
            {
                ClientMailBoxEdit.UpdateAsync(
                    DataEditMailBox._Subject,
                    DataEditMailBox._From,
                    DataEditMailBox._Date,
                    DataEditMailBox._Size,
                    DataEditMailBox._To,
                    true,
                    DataEditMailBox._Header,
                    DataEditMailBox._ID);
            }

            DataGrid1.SelectedItem = DataForm2.CurrentItem;
       //     CheckBox r = DataForm2.CurrentItem as CheckBox;
       //     r.IsChecked = true;
        
        }

        private void DataForm2_DeletingItem(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MailboxClass DataEditIPL = DataForm2.CurrentItem as MailboxClass;

            ClientMailBoxEdit.DeleteAsync(DataEditIPL._ID);
            EmailCount = EmailCount - 1;
            ButtonCheckMail.Content = "(" + "پیغام ها" + " (" + EmailCount;
   
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            DataForm2.DeleteItem();
        }
    }
}
