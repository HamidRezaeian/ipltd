﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace IPLTD.Windows
{
	public partial class Style : UserControl
	{
		public Style()
		{
			InitializeComponent();
        //    this.Loaded += new RoutedEventHandler(Style_Loaded);
        }

      //  void Style_Loaded(object sender, RoutedEventArgs e)
      //  {
     //       Window1.Width = 500;
       //     Window1.MinHeight = 400;
      //  }

//-------------------------------------------------------------------------------------------------------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        public void Window_Close()
        {
            
            Window_RestoreSize();
            Window1.Width = 0;
        }
        public void Window_Show()
        {
            this.Visibility = Visibility.Visible;
            double Left = (LayoutRoot.ActualWidth / 2);
            double Top = (LayoutRoot.ActualHeight / 2);
            Left = Left / 2;
            Top = Top / 2;
            Drag1.X = Left;
            Drag1.Y = Top;
            Window1.Width = LayoutRoot.ActualWidth - (Left * 2);
            Window1.Height = LayoutRoot.ActualHeight - (Top * 2);
            if ((Window1.Width < 400) && (Window1.Height < 400))
            {
                Drag1.X = 0;
                Drag1.Y = Window1Border.Height;
                Window1.Width = LayoutRoot.ActualWidth;
                Window1.Height = LayoutRoot.ActualHeight - Window1Border.Height;
            }
            if (Window1Border.Width<Window1.ActualWidth)
            {
                Window1Border.Width = Window1.ActualWidth;
            }
            Drag1_Dragging(Window1Border, null);
            StoryboardLoadWindow.Begin();
 
        }

        
        public bool BoolMaximize;
        Point UndoPosition;
        double UndoWidth, UndoHeight;
        private void Window_Maximize()
        {

                UndoPosition.X = Drag1.X;
                UndoPosition.Y = Drag1.Y;
                UndoWidth = Window1.Width;
                UndoHeight = Window1.Height;
                Drag1.X = 0;
                Drag1.Y = Window1Border.Height;
                Drag1_Dragging(Window1Border, null);
                Window1.Width = LayoutRoot.ActualWidth;
                Window1.Height = LayoutRoot.ActualHeight - Window1Border.Height;
                BoolMaximize = true;
                Resize1.Detach();
                ButtonMaximize.Visibility = Visibility.Collapsed;
                ButtonRestore.Visibility = Visibility.Visible;
                Drag1.Detach();
        }
      //  double Left;
        
        private void Window_RestoreSize()
        {
            Window1.Width = UndoWidth;
            Window1.Height = UndoHeight;
            Resize1.Attach(Window1);
            BoolMaximize = false;
            ButtonMaximize.Visibility = Visibility.Visible;
            ButtonRestore.Visibility = Visibility.Collapsed;
            Resize1.StayInParent = false;



            
            Drag1.Attach(Window1Border);
            Drag1_Dragging(Window1Border, null);
        
        }
        private void Fix_Position_Resize()
        {
            if (Window1.Width > LayoutRoot.ActualWidth)
            {
                Window1.Width = LayoutRoot.ActualWidth;
                Window1Border.Width = LayoutRoot.ActualWidth;
                Window1.Height = LayoutRoot.ActualHeight;
            }
                Window1.SetValue(Canvas.LeftProperty, Drag1.X);
                Window1.SetValue(Canvas.TopProperty, Drag1.Y);
                Resize1.HWSdrag.X = Drag1.X;
                Resize1.HWSdrag.Y = Drag1.Y;
                Resize1.HWSElement = Window1;
        }
//-------------------------------------------------------------------------------------------------------
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        private void Resize1_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                Resize1.StayInParent = true;
            }
            catch { }
        }
        private void Drag1_Dragging(object sender, MouseEventArgs e)
        {
            Fix_Position_Resize();
        }
        private void LayoutRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try

            {
                if (BoolMaximize == true)
                {
                    Resize1.StayInParent = true;
                    Window1.Width = LayoutRoot.ActualWidth;
                    Window1.Height = LayoutRoot.ActualHeight - Window1Border.Height;
                    Drag1.X = 0;
                    Drag1.Y = Window1Border.Height;
                    Drag1_Dragging(Window1Border, null);

                }
                else
                {
                    Resize1.Detach();
                    Resize1.StayInParent = false;
                    Resize1.Attach(Window1);
                }
            }
            catch { }
        }
        private void ButtonMaximize_Click(object sender, RoutedEventArgs e)
        {
                Window_Maximize();
        }
        private void ButtonRestore_Click(object sender, RoutedEventArgs e)
        {
            Window_RestoreSize();
            
         //   Window1Border.SetValue(Canvas.LeftProperty, UndoPosition.X);

//             Thickness a = new Thickness();
           //  a.Left = UndoPosition.X+(Window1Border.Width/2);
            // a.Top = UndoPosition.Y;

  //           Window1Border.Margin = a; 

            
        }
        private void Window1Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
        }
        private void Drag1_DragBegun(object sender, MouseEventArgs e)
        {
        }

        private void UserControl1_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
             Window_Close();
        }

        private void ButtonMinimize_Click(object sender, RoutedEventArgs e)
        {
            if (BoolMaximize == true)
            {
                Window_RestoreSize();
            }
           StoryboardMinimizeWindow.Begin();
        }

        

  
        
        
        

	}
}