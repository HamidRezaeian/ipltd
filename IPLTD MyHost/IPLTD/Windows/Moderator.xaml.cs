﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace IPLTD.Windows
{
    public partial class Moderator : UserControl
    {
        public Moderator()
        {
            InitializeComponent();
            style1.Window1.SizeChanged += new SizeChangedEventHandler(Window1_SizeChanged);
            style1.Drag1.Dragging += new MouseEventHandler(Drag1_Dragging);
            style1.ButtonClose.Click += new RoutedEventHandler(ButtonClose_Click);
            style1.ButtonMinimize.Click += new RoutedEventHandler(ButtonMinimize_Click);
            SetSize();

            //  ButtonClose_Click(null, null);

            WindowSource.Visibility = Visibility.Collapsed;
            this.Opacity = 0;
        }
        public Top Toolbar_Top { get; set; }
        void ButtonMinimize_Click(object sender, RoutedEventArgs e)
        {
            StoryboardMinimizeWindow.Begin();
            Toolbar_Top.Minimize_panel_WindowShow(Toolbar_Top.buttonModerator);
            LoginPage1.LOST_Focus(LoginPage1.Moderator1);
        }



        //------------------------------------------------------------------------------
        //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

        public LoginPage LoginPage1 { get; set; }

        public void LoadWindow()
        {
            Toolbar_Top.Minimize_panel_WindowShow(Toolbar_Top.buttonModerator);

            LoginPage1.SET_Focus(LoginPage1.Moderator1);
            style1.Window_Show();
            SetPosition();
            StoryboardLoadWindowSource.Begin();
            WindowSource.Visibility = Visibility.Visible;
            this.Opacity = 1;
            style1.WindowCaption_Text.Text = "مدیریت";
        }

        void ButtonClose_Click(object sender, RoutedEventArgs e)
        {

            LoginPage1.Close_Focus(LoginPage1.Moderator1);
            Toolbar_Top.Minimize_panel_WindowHide(Toolbar_Top.buttonModerator);

            WindowSource.Visibility = Visibility.Collapsed;
            this.Opacity = 0;
        }

        public void SetPosition()
        {
            Thickness a = new Thickness(style1.Drag1.X + 7, style1.Drag1.Y + 4, 0, 0);
            WindowSource.Margin = a;

        }
        void SetSize()
        {
            WindowSource.Width = style1.Window1.Width - 14;
            WindowSource.Height = style1.Window1.Height - 11;

        }

        void Drag1_Dragging(object sender, MouseEventArgs e)
        {
            SetPosition();
        }



        void Window1_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetPosition();
            SetSize();
        }

        private void LayoutRoot_SizeChanged(object sender, SizeChangedEventArgs e)
        {
        }
    }
}
