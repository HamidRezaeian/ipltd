﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace IPLTD
{
    public static class Message
    {

        public static void ErrorMessage(string message)
        {
            new CustomMessage(message, CustomMessage.MessageType.Error).Show();
        }

        public static void InfoMessage(string message)
        {
            new CustomMessage(message, CustomMessage.MessageType.Info).Show();
        }


    }
}