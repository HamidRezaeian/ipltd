﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using IPLTD.Windows;
using System.Threading;

namespace IPLTD 
{
	public partial class Top : UserControl
	{
        string StrTime;
       
        System.Windows.Threading.DispatcherTimer _timer;
        public Top()
		{

         

        	InitializeComponent();


            _timer = new System.Windows.Threading.DispatcherTimer();
            _timer.Tick += new EventHandler(_timer_Tick);
            _timer.Interval = new TimeSpan(0, 0, 1);
            _timer.Start();
           

            Gadget_Calendar.Checked+=new RoutedEventHandler(Gadget_Calendar_Checked);
            Gadget_Calendar.Unchecked+=new RoutedEventHandler(Gadget_Calendar_Unchecked);
            Gadget_Clock.Checked+=new RoutedEventHandler(Gadget_Clock_Checked);
            Gadget_Clock.Unchecked+=new RoutedEventHandler(Gadget_Clock_Unchecked);
            StoryboardMailRIcon.Begin();
            StrTime = Convert.ToString(DateTime.Now);
            ShowTime.Text = StrTime;
            buttonIPL.Visibility = Visibility.Collapsed;
            buttonInfo.Visibility = Visibility.Collapsed;
            buttonModerator.Visibility = Visibility.Collapsed;
            buttonMailBox.Visibility = Visibility.Collapsed;
            buttonAbout.Visibility = Visibility.Collapsed;
        }


        void _timer_Tick(object sender, EventArgs e)
        {
            
            StrTime = Convert.ToString(DateTime.Now);
            ShowTime.Text = StrTime;
        }
        int Position=0;
        Thickness Thick = new Thickness(0, 0, 0, 0);
        public void Minimize_panel_WindowShow(Button sender)
        {
            if (sender.Visibility == Visibility.Collapsed)
            {
                Thick.Left = Position;
                sender.Visibility = Visibility.Visible;
                sender.Margin = Thick;
                Position = Position + 50;
            }
        }
        public void Minimize_panel_WindowHide(Button sender)
        {

            if (sender.Margin.Left != 0)
            {
                Thick.Left = sender.Margin.Left + 50;
                sender.Margin = Thick;
            }
            sender.Visibility = Visibility.Collapsed;
            

            if (sender.Margin.Left != Position)
            {
                if (buttonIPL.Margin.Left >= 50)
                {
                    Thick.Left = buttonIPL.Margin.Left - 50;
                    buttonIPL.Margin = Thick;
                }
                if (buttonInfo.Margin.Left >= 50)
                {
                    Thick.Left = buttonInfo.Margin.Left - 50;
                    buttonInfo.Margin = Thick;
                }
                if (buttonModerator.Margin.Left >= 50)
                {
                    Thick.Left = buttonModerator.Margin.Left - 50;
                    buttonModerator.Margin = Thick;
                }
                if (buttonMailBox.Margin.Left >= 50)
                {
                    Thick.Left = buttonMailBox.Margin.Left - 50;
                    buttonMailBox.Margin = Thick;
                }
                if (buttonAbout.Margin.Left >= 50)
                {
                    Thick.Left = buttonAbout.Margin.Left - 50;
                    buttonAbout.Margin = Thick;
                }
            }

            Position = Position - 50;


        }



        private void InstallButton_Click(object sender, RoutedEventArgs e)
        {
            if (Application.Current.InstallState == InstallState.NotInstalled)
            {
                Application.Current.Install();
            }
            else
            {
                Message.InfoMessage("نرم افزار قبلا بر روی سیستم شما نصب شده است!");
            }

        }
        bool JustOneTime = false;
        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
           
        
            if (Application.Current.IsRunningOutOfBrowser == true)
            {
                JustOneTime = true;
                Application.Current.CheckAndDownloadUpdateCompleted += new CheckAndDownloadUpdateCompletedEventHandler(Current_CheckAndDownloadUpdateCompleted);
                Application.Current.CheckAndDownloadUpdateAsync();

            }
            else
            {
                Message.InfoMessage("این امکان فقط در اجرای خارج از مرورگر امکان پذیر میباشد ");
            }
        }
        int num;
        void Current_CheckAndDownloadUpdateCompleted(object sender, CheckAndDownloadUpdateCompletedEventArgs e)
        {
            
            if (e.Error == null)
            {
                if (e.UpdateAvailable == true)
                {
                    num = 1;
                }
                else
                {
                    num = 2;
                }
            }
            else
            {
                num = 3;
            }


            MessageResult(num);
        }

        void MessageResult(int ErrorNum)
        {
            if (JustOneTime == true)
            {
                if (ErrorNum == 1)
                    Message.InfoMessage("بروز رسانی با موفقیت انجام شد! جهت مشاهده تغییرات نرم افزار را مجددا اجرا فرمایید ");

                else if (ErrorNum == 2)
                    Message.InfoMessage("بروز رسانی جدید یافت نشد!");
                else if (ErrorNum == 3)
                    Message.ErrorMessage("در روند بروز رسانی خطایی رخ داده است!");
            }

            JustOneTime = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (borderGadget.Visibility == Visibility.Collapsed)
            StoryboardGadgets.Begin();
            else
            borderGadget.Visibility = Visibility.Collapsed;
        }

        public IPLT WindowIPLTop { get; set; }
        public Personal WindowPersonalTop { get; set; }
        public Moderator WindowModeratorTop { get; set; }
        public MailBox WindowMailTop { get; set; }
        public About WindowAboutTop { get; set; }


        private void buttonIPL_Click(object sender, RoutedEventArgs e)
        {
            if (WindowIPLTop.WindowSource.Visibility == Visibility.Collapsed)
            {
                WindowIPLTop.WindowSource.Visibility = Visibility.Visible;
                WindowIPLTop.style1.Visibility = Visibility.Visible;
                WindowIPLTop.style1.Opacity = 1;

            }
            else
            {
                WindowIPLTop.style1.StoryboardMinimizeWindow.Begin();
                WindowIPLTop.StoryboardMinimizeWindow.Begin();

            }
        }
        private void buttonInfo_Click(object sender, RoutedEventArgs e)
        {
            if (WindowPersonalTop.WindowSource.Visibility == Visibility.Collapsed)
            {
                WindowPersonalTop.WindowSource.Visibility = Visibility.Visible;
                WindowPersonalTop.style1.Visibility = Visibility.Visible;
                WindowPersonalTop.style1.Opacity = 1;

            }
            else
            {
                WindowPersonalTop.style1.StoryboardMinimizeWindow.Begin();
                WindowPersonalTop.StoryboardMinimizeWindow.Begin();

            }
        }

        private void buttonModerator_Click(object sender, RoutedEventArgs e)
        {
            if (WindowModeratorTop.WindowSource.Visibility == Visibility.Collapsed)
            {
                WindowModeratorTop.WindowSource.Visibility = Visibility.Visible;
                WindowModeratorTop.style1.Visibility = Visibility.Visible;
                WindowModeratorTop.style1.Opacity = 1;

            }
            else
            {
                WindowModeratorTop.style1.StoryboardMinimizeWindow.Begin();
                WindowModeratorTop.StoryboardMinimizeWindow.Begin();

            }
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            if (StartMenu.Visibility == Visibility.Collapsed)
            {
                StoryboardShowStartMenu.Begin();
            }
            else
            {
                StartMenu.Visibility = Visibility.Collapsed;
            }
         }

        public LoginPage WindowLoginPageTop { get; set; }
        private void ButtonSwichUser_Click(object sender, RoutedEventArgs e)
        {
            StartMenu.Visibility = Visibility.Collapsed;
            WindowLoginPageTop.StoryboardSwichUser.Begin();
        }

        private void ButtonStartMenu_Email_Click(object sender, RoutedEventArgs e)
        {
            if (WindowMailTop.Opacity == 0)
                WindowMailTop.LoadWindow();
        }

        private void ButtonStartMenu_IPL_Click(object sender, RoutedEventArgs e)
        {
            if (WindowIPLTop.Opacity == 0)
                WindowIPLTop.LoadWindow();
        }

        private void ButtonStartMenu_Info_Click(object sender, RoutedEventArgs e)
        {
            if (WindowPersonalTop.Opacity == 0)
                WindowPersonalTop.LoadWindow();

        }

        private void ButtonStartMenu_Moderator_Click(object sender, RoutedEventArgs e)
        {
            if (WindowPersonalTop.WindowSource.CheckboxModerator.IsChecked == true)
            {
                if (WindowModeratorTop.Opacity == 0)

                    WindowModeratorTop.LoadWindow();
            }
            else
                Message.ErrorMessage("کاربر گرامی : شما مجوز ورود به این قسمت را ندارید.");
        }

        private void buttonMailBox_Click(object sender, RoutedEventArgs e)
        {
            if (WindowMailTop.WindowSource.Visibility == Visibility.Collapsed)
            {
                WindowMailTop.WindowSource.Visibility = Visibility.Visible;
                WindowMailTop.style1.Visibility = Visibility.Visible;
                WindowMailTop.style1.Opacity = 1;
            }
            else
            {
                WindowMailTop.style1.StoryboardMinimizeWindow.Begin();
                WindowMailTop.StoryboardMinimizeWindow.Begin();

            }

        }

        
        private void ButtonReciveMailIcon_Click(object sender, RoutedEventArgs e)
        {
            StoryboardMailRIcon.Stop();
            Parent_ButtonReciveMailIcon.Visibility = Visibility.Collapsed;
            if (WindowMailTop.Opacity == 0)
                WindowMailTop.LoadWindow();
        }

        private void Gadget_Clock_Checked(object sender, RoutedEventArgs e)
        {
            WindowLoginPageTop.clock1.Visibility = Visibility.Visible;
        }

        private void Gadget_Clock_Unchecked(object sender, RoutedEventArgs e)
        {
            WindowLoginPageTop.clock1.Visibility = Visibility.Collapsed;
        }

        private void Gadget_Calendar_Checked(object sender, RoutedEventArgs e)
        {
            WindowLoginPageTop.Calendar1.Visibility = Visibility.Visible;
        }

        private void Gadget_Calendar_Unchecked(object sender, RoutedEventArgs e)
        {
            WindowLoginPageTop.Calendar1.Visibility = Visibility.Collapsed;
        }

        private void ButtonStartMenu_About_Click(object sender, RoutedEventArgs e)
        {
            if (WindowAboutTop.Opacity == 0)
                WindowAboutTop.LoadWindow();

        }

        private void buttonAbout_Click(object sender, RoutedEventArgs e)
        {
            if (WindowAboutTop.WindowSource.Visibility == Visibility.Collapsed)
            {
                WindowAboutTop.WindowSource.Visibility = Visibility.Visible;
                WindowAboutTop.style1.Visibility = Visibility.Visible;
                WindowAboutTop.style1.Opacity = 1;
            }
            else
            {
                WindowAboutTop.style1.StoryboardMinimizeWindow.Begin();
                WindowAboutTop.StoryboardMinimizeWindow.Begin();

            }

        }



 
	}
}