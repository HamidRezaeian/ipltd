﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using IPLTD.Windows;
namespace IPLTD
{
	public partial class Footer : UserControl
	{
        public Footer()
		{
        	InitializeComponent();
        }

        public IPLT WindowIPL { get; set; }
        public Personal WindowPersonal { get; set; }
        public Moderator WindowModerator { get; set; }
        private void ButtonIPL_Click(object sender, RoutedEventArgs e)
        {
            if (WindowIPL.Opacity==0)
                WindowIPL.LoadWindow();
        }
        
        private void ButtonModerator_Click(object sender, RoutedEventArgs e)
        {
            if (WindowPersonal.WindowSource.CheckboxModerator.IsChecked == true)
            {
                if (WindowModerator.Opacity==0)

                    WindowModerator.LoadWindow();
            }
            else
                Message.ErrorMessage("کاربر گرامی : شما مجوز ورود به این قسمت را ندارید.");
        }

        private void ButtonInformation_Click(object sender, RoutedEventArgs e)
        {
            if (WindowPersonal.Opacity == 0)

            WindowPersonal.LoadWindow();
        }
        
     
	}
}