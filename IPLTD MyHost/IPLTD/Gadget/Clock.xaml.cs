﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;

namespace IPLTD
{
    public partial class Clock : UserControl, INotifyPropertyChanged
    {
        private DispatcherTimer _dayTimer;
        private string _currentMonth;
        private string _currentDay;

        public Clock()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);

            this.DataContext = this;
        }

        public string CurrentMonth
        {
            get { return _currentMonth; }
            set
            {
                _currentMonth = value;
                OnPropertyChanged("CurrentMonth");
            }
        }

        public string CurrentDay
        {
            get { return _currentDay; }
            set
            {
                _currentDay = value;
                OnPropertyChanged("CurrentDay");
            }
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            // set the datacontext to be today's date
            DateTime now = DateTime.Now;
            CurrentDay = now.Day.ToString();
            CurrentMonth = now.ToString("MMM");

            //set up a timer to fire at the start of tomorrow
            _dayTimer = new DispatcherTimer();
            _dayTimer.Interval = new TimeSpan(1, 0, 0, 0) - now.TimeOfDay;
            _dayTimer.Tick += new EventHandler(OnDayChange);
            _dayTimer.Start();

            // seek the timeline, which assumes a beginning at midnight, to the appropriate offset
            Storyboard sb = (Storyboard)this.Resources["sb"];
            sb.Begin();
            sb.Seek(now.TimeOfDay);
        }

        private void OnDayChange(object sender, EventArgs e)
        {
            // date has changed, update the datacontext to reflect today's date
            DateTime now = DateTime.Now;
            CurrentDay = now.Day.ToString();
            CurrentMonth = now.ToString("MMM");
            //now fire every 24 hours
            _dayTimer.Interval = new TimeSpan(1, 0, 0, 0);
        }

        #region Helper Methods
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        #endregion

    }
}
