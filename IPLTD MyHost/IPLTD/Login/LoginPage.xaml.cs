﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Interactivity;
using IPLTD.ServiceReferenceLogin;
using IPLTD.Class.Security;
using IPLTD.Windows;
namespace IPLTD
{
	public partial class LoginPage : UserControl
	{
        private ServiceLoginClient ClientLogin = new ServiceLoginClient();
		public LoginPage()
		{
            InitializeComponent();

     

            ClientLogin.LoginCompleted += new EventHandler<LoginCompletedEventArgs>(ClientLogin_LoginCompleted);


            IPLT1.WindowSource.Loginpage1 = this;

            
            IPLT1.LoginPage1 = this;
            Moderator1.LoginPage1 = this;
            Personal1.LoginPage1 = this;
            MailBox1.LoginPage1 = this;
            About1.LoginPage1 = this;


            Moderator1.Toolbar_Top = Top1;
            IPLT1.Toolbar_Top = Top1;
            Personal1.Toolbar_Top = Top1;
            MailBox1.Toolbar_Top = Top1;
            MailBox1.WindowSource.Mytop = Top1;
            About1.Toolbar_Top = Top1;


            Footer1.WindowIPL = IPLT1;
            Footer1.WindowPersonal = Personal1;
            Footer1.WindowModerator = Moderator1;

            Top1.WindowLoginPageTop = this;
            Top1.WindowIPLTop = IPLT1;
            Top1.WindowModeratorTop = Moderator1;
            Top1.WindowPersonalTop = Personal1;
            Top1.WindowMailTop = MailBox1;
            Top1.WindowAboutTop = About1;

            Moderator1.WindowSource.WindowMailboxSource = MailBox1.WindowSource;
            this.WindowPersonalSource = Personal1.WindowSource;
            this.WindowMailboxSource = MailBox1.WindowSource;

            MailBox1.WindowSource.WindowPersonalSource = Personal1.WindowSource;
            IPLT1.WindowSource.WindowPersonal = Personal1;
		}

        
        int Accesslvl;
        public Personal_Source_ WindowPersonalSource { get; set; }
        public MailBox_Source_ WindowMailboxSource { get; set; }
       void ClientLogin_LoginCompleted(object sender, LoginCompletedEventArgs e)
        {

            Loading_Login.Visibility = Visibility.Collapsed;
            if (e.Result.Access==true)
            {
                Accesslvl = 0;
                AnimationLogin.Begin();
                WindowPersonalSource.LabelUsername.Content = e.Result.UserName;
                WindowPersonalSource.LabelFamily.Content = e.Result.Family;
                WindowPersonalSource.LabelName.Content = e.Result.Name;
                WindowPersonalSource.LabelState.Content = e.Result.State;
                WindowPersonalSource.CheckboxState1.IsChecked = e.Result.AccessEdit_State1;
                WindowPersonalSource.CheckboxState2.IsChecked = e.Result.AccessEdit_State2;
                WindowPersonalSource.CheckboxState3.IsChecked = e.Result.AccessEdit_State3;
                WindowPersonalSource.CheckboxState4.IsChecked = e.Result.AccessEdit_State4;
                WindowPersonalSource.CheckboxState5.IsChecked = e.Result.AccessEdit_State5;
                WindowPersonalSource.CheckboxState6.IsChecked = e.Result.AccessEdit_State6;
                WindowPersonalSource.CheckboxState7.IsChecked = e.Result.AccessEdit_State7;
            
                WindowPersonalSource.CheckboxModerator.IsChecked = e.Result.Manager;
                WindowMailboxSource.ClientMailBox.GetAllInformationAsync(e.Result.UserName);
                if (e.Result.AccessEdit_State1 == true)
                    Accesslvl++;
                if (e.Result.AccessEdit_State2 == true)
                    Accesslvl++;
                if (e.Result.AccessEdit_State3 == true)
                    Accesslvl++;
                if (e.Result.AccessEdit_State4 == true)
                    Accesslvl++;
                if (e.Result.AccessEdit_State5 == true)
                    Accesslvl++;
                if (e.Result.AccessEdit_State6 == true)
                    Accesslvl++;
                if (e.Result.Manager == true)
                    Accesslvl = Accesslvl + 4;
                if (Accesslvl < 1)
                {
                    WindowPersonalSource.LabelAccessLevel.Content = "حداقل";
                }
                else if (Accesslvl < 2)
                {
                    WindowPersonalSource.LabelAccessLevel.Content = "پایین";
                }
                else if (Accesslvl <= 6)
                {
                    WindowPersonalSource.LabelAccessLevel.Content = "متوسط";
                }
                else if (Accesslvl > 6)
                {
                    WindowPersonalSource.LabelAccessLevel.Content = "بالا";
                }
            }
            else
            {
                TextBoxUserName.IsEnabled = true;
                passwordBox.IsEnabled = true;
                label_Error_Login.Visibility = Visibility.Visible;
            }

        }
        private void passwordBox_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.Key==Key.Enter)
            {
                AnimationLogin.Begin();
              //  Login_Go();
            }
        }

        void Login_Go()
        {

            label_Error_Login.Visibility = Visibility.Collapsed;
            Loading_Login.Visibility = Visibility.Visible;
            SaltedHash Salt = new SaltedHash();
            byte[] password;
            TextBoxUserName.IsEnabled = false;
            passwordBox.IsEnabled = false;

            password = Salt.hash(passwordBox.Password);
            ClientLogin.LoginAsync(TextBoxUserName.Text, password);
            //    AnimationLogin.Begin();

        }



        private void IPLT1_LostFocus(object sender, RoutedEventArgs e)
        {
            LOST_Focus(IPLT1);

        }

        private void IPLT1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SET_Focus(IPLT1);
        }

        private void IPLT1_GotFocus(object sender, RoutedEventArgs e)
        {
            SET_Focus(IPLT1);
        }
        
   
        private void Moderator1_GotFocus(object sender, RoutedEventArgs e)
        {
            SET_Focus(Moderator1);
        }

        private void Moderator1_LostFocus(object sender, RoutedEventArgs e)
        {
            LOST_Focus(Moderator1);
        }

        private void Moderator1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SET_Focus(Moderator1);
        }





        private void Personal1_GotFocus(object sender, RoutedEventArgs e)
        {
            SET_Focus(Personal1);
        }

        private void Personal1_LostFocus(object sender, RoutedEventArgs e)
        {
            LOST_Focus(Personal1);
        }

        private void Personal1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SET_Focus(Personal1);
        }

        public void SET_Focus(UserControl u)
        {
            IPLT1.SetValue(Canvas.ZIndexProperty, 2);
            Personal1.SetValue(Canvas.ZIndexProperty, 2);
            Moderator1.SetValue(Canvas.ZIndexProperty, 2);
            MailBox1.SetValue(Canvas.ZIndexProperty, 2);
            About1.SetValue(Canvas.ZIndexProperty, 2);

            if (IPLT1.Opacity==0)
                IPLT1.SetValue(Canvas.ZIndexProperty, 1);
            if (Personal1.Opacity == 0)
                Personal1.SetValue(Canvas.ZIndexProperty, 1);
            if (Moderator1.Opacity == 0)
                Moderator1.SetValue(Canvas.ZIndexProperty, 1);
            if (MailBox1.Opacity == 0)
                MailBox1.SetValue(Canvas.ZIndexProperty, 1);
            if (About1.Opacity == 0)
                About1.SetValue(Canvas.ZIndexProperty, 1);

            u.SetValue(Canvas.ZIndexProperty, 4);
        }

        public void LOST_Focus(UserControl u)
        {
            u.SetValue(Canvas.ZIndexProperty, 1);
        }

        public void Close_Focus(UserControl u)
        {
            u.SetValue(Canvas.ZIndexProperty, 1);
        }

        private void MailBox1_GotFocus(object sender, RoutedEventArgs e)
        {
            SET_Focus(MailBox1);
        }

        private void MailBox1_LostFocus(object sender, RoutedEventArgs e)
        {
            LOST_Focus(MailBox1);
        }

        private void MailBox1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SET_Focus(MailBox1);
        }

        private void About1_GotFocus(object sender, RoutedEventArgs e)
        {
            SET_Focus(About1);
        }

        private void About1_LostFocus(object sender, RoutedEventArgs e)
        {
            LOST_Focus(About1);
        }

        private void About1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SET_Focus(About1);
        }

    }


	
}