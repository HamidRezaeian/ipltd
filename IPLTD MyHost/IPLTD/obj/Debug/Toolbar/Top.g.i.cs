﻿#pragma checksum "C:\Users\hamid\Documents\Visual Studio 2010\Projects\IPLTD MyHost\IPLTD\Toolbar\Top.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "1C8B18C14681A713ADA5EDA194E24594"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.225
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace IPLTD {
    
    
    public partial class Top : System.Windows.Controls.UserControl {
        
        internal System.Windows.Media.Animation.Storyboard StoryboardGadgets;
        
        internal System.Windows.Media.Animation.Storyboard StoryboardShowStartMenu;
        
        internal System.Windows.Media.Animation.Storyboard StoryboardMailRIcon;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Shapes.Rectangle rectangle;
        
        internal System.Windows.Controls.TextBlock ShowTime;
        
        internal System.Windows.Controls.Border StartMenu;
        
        internal System.Windows.Controls.Button ButtonStartMenu_IPL;
        
        internal System.Windows.Controls.Button ButtonStartMenu_Info;
        
        internal System.Windows.Controls.Button ButtonStartMenu_Moderator;
        
        internal System.Windows.Controls.Button ButtonStartMenu_Email;
        
        internal System.Windows.Controls.Button ButtonStartMenu_About;
        
        internal System.Windows.Controls.Button ButtonSwichUser;
        
        internal System.Windows.Controls.Button ButtonStart;
        
        internal System.Windows.Controls.Button buttonIPL;
        
        internal System.Windows.Controls.Button buttonInfo;
        
        internal System.Windows.Controls.Button buttonModerator;
        
        internal System.Windows.Controls.Button buttonMailBox;
        
        internal System.Windows.Controls.Button buttonAbout;
        
        internal System.Windows.Controls.Button InstallButton;
        
        internal System.Windows.Controls.Button UpdateButton;
        
        internal System.Windows.Controls.Border borderGadget;
        
        internal System.Windows.Controls.CheckBox Gadget_Clock;
        
        internal System.Windows.Controls.CheckBox Gadget_Calendar;
        
        internal System.Windows.Controls.Border Parent_ButtonReciveMailIcon;
        
        internal System.Windows.Controls.Grid grid;
        
        internal System.Windows.Controls.Button ButtonReciveMailIcon;
        
        internal System.Windows.Controls.TextBlock Label_MailCount;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/IPLTD;component/Toolbar/Top.xaml", System.UriKind.Relative));
            this.StoryboardGadgets = ((System.Windows.Media.Animation.Storyboard)(this.FindName("StoryboardGadgets")));
            this.StoryboardShowStartMenu = ((System.Windows.Media.Animation.Storyboard)(this.FindName("StoryboardShowStartMenu")));
            this.StoryboardMailRIcon = ((System.Windows.Media.Animation.Storyboard)(this.FindName("StoryboardMailRIcon")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.rectangle = ((System.Windows.Shapes.Rectangle)(this.FindName("rectangle")));
            this.ShowTime = ((System.Windows.Controls.TextBlock)(this.FindName("ShowTime")));
            this.StartMenu = ((System.Windows.Controls.Border)(this.FindName("StartMenu")));
            this.ButtonStartMenu_IPL = ((System.Windows.Controls.Button)(this.FindName("ButtonStartMenu_IPL")));
            this.ButtonStartMenu_Info = ((System.Windows.Controls.Button)(this.FindName("ButtonStartMenu_Info")));
            this.ButtonStartMenu_Moderator = ((System.Windows.Controls.Button)(this.FindName("ButtonStartMenu_Moderator")));
            this.ButtonStartMenu_Email = ((System.Windows.Controls.Button)(this.FindName("ButtonStartMenu_Email")));
            this.ButtonStartMenu_About = ((System.Windows.Controls.Button)(this.FindName("ButtonStartMenu_About")));
            this.ButtonSwichUser = ((System.Windows.Controls.Button)(this.FindName("ButtonSwichUser")));
            this.ButtonStart = ((System.Windows.Controls.Button)(this.FindName("ButtonStart")));
            this.buttonIPL = ((System.Windows.Controls.Button)(this.FindName("buttonIPL")));
            this.buttonInfo = ((System.Windows.Controls.Button)(this.FindName("buttonInfo")));
            this.buttonModerator = ((System.Windows.Controls.Button)(this.FindName("buttonModerator")));
            this.buttonMailBox = ((System.Windows.Controls.Button)(this.FindName("buttonMailBox")));
            this.buttonAbout = ((System.Windows.Controls.Button)(this.FindName("buttonAbout")));
            this.InstallButton = ((System.Windows.Controls.Button)(this.FindName("InstallButton")));
            this.UpdateButton = ((System.Windows.Controls.Button)(this.FindName("UpdateButton")));
            this.borderGadget = ((System.Windows.Controls.Border)(this.FindName("borderGadget")));
            this.Gadget_Clock = ((System.Windows.Controls.CheckBox)(this.FindName("Gadget_Clock")));
            this.Gadget_Calendar = ((System.Windows.Controls.CheckBox)(this.FindName("Gadget_Calendar")));
            this.Parent_ButtonReciveMailIcon = ((System.Windows.Controls.Border)(this.FindName("Parent_ButtonReciveMailIcon")));
            this.grid = ((System.Windows.Controls.Grid)(this.FindName("grid")));
            this.ButtonReciveMailIcon = ((System.Windows.Controls.Button)(this.FindName("ButtonReciveMailIcon")));
            this.Label_MailCount = ((System.Windows.Controls.TextBlock)(this.FindName("Label_MailCount")));
        }
    }
}

