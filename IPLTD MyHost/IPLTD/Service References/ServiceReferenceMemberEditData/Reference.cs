﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.208
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This code was auto-generated by Microsoft.Silverlight.ServiceReference, version 4.0.50826.0
// 
namespace IPLTD.ServiceReferenceMemberEditData {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="", ConfigurationName="ServiceReferenceMemberEditData.ServiceMemberEditData")]
    public interface ServiceMemberEditData {
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="urn:ServiceMemberEditData/Insert", ReplyAction="urn:ServiceMemberEditData/InsertResponse")]
        System.IAsyncResult BeginInsert(string Name, string Family, string Username, byte[] Password, bool AccessEdit_State1, bool AccessEdit_State2, bool AccessEdit_State3, bool AccessEdit_State4, bool AccessEdit_State5, bool AccessEdit_State6, bool Manager, int State, bool AccessEdit_State7, System.AsyncCallback callback, object asyncState);
        
        void EndInsert(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="urn:ServiceMemberEditData/Edit", ReplyAction="urn:ServiceMemberEditData/EditResponse")]
        System.IAsyncResult BeginEdit(
                    string Name, 
                    string Family, 
                    string Username, 
                    byte[] Password, 
                    bool AccessEdit_State1, 
                    bool AccessEdit_State2, 
                    bool AccessEdit_State3, 
                    bool AccessEdit_State4, 
                    bool AccessEdit_State5, 
                    bool AccessEdit_State6, 
                    bool Manager, 
                    int State, 
                    long ID, 
                    bool AccessEdit_State7, 
                    System.AsyncCallback callback, 
                    object asyncState);
        
        void EndEdit(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="urn:ServiceMemberEditData/Delete", ReplyAction="urn:ServiceMemberEditData/DeleteResponse")]
        System.IAsyncResult BeginDelete(long ID, System.AsyncCallback callback, object asyncState);
        
        void EndDelete(System.IAsyncResult result);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ServiceMemberEditDataChannel : IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceMemberEditDataClient : System.ServiceModel.ClientBase<IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData>, IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData {
        
        private BeginOperationDelegate onBeginInsertDelegate;
        
        private EndOperationDelegate onEndInsertDelegate;
        
        private System.Threading.SendOrPostCallback onInsertCompletedDelegate;
        
        private BeginOperationDelegate onBeginEditDelegate;
        
        private EndOperationDelegate onEndEditDelegate;
        
        private System.Threading.SendOrPostCallback onEditCompletedDelegate;
        
        private BeginOperationDelegate onBeginDeleteDelegate;
        
        private EndOperationDelegate onEndDeleteDelegate;
        
        private System.Threading.SendOrPostCallback onDeleteCompletedDelegate;
        
        private BeginOperationDelegate onBeginOpenDelegate;
        
        private EndOperationDelegate onEndOpenDelegate;
        
        private System.Threading.SendOrPostCallback onOpenCompletedDelegate;
        
        private BeginOperationDelegate onBeginCloseDelegate;
        
        private EndOperationDelegate onEndCloseDelegate;
        
        private System.Threading.SendOrPostCallback onCloseCompletedDelegate;
        
        public ServiceMemberEditDataClient() {
        }
        
        public ServiceMemberEditDataClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceMemberEditDataClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceMemberEditDataClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceMemberEditDataClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Net.CookieContainer CookieContainer {
            get {
                System.ServiceModel.Channels.IHttpCookieContainerManager httpCookieContainerManager = this.InnerChannel.GetProperty<System.ServiceModel.Channels.IHttpCookieContainerManager>();
                if ((httpCookieContainerManager != null)) {
                    return httpCookieContainerManager.CookieContainer;
                }
                else {
                    return null;
                }
            }
            set {
                System.ServiceModel.Channels.IHttpCookieContainerManager httpCookieContainerManager = this.InnerChannel.GetProperty<System.ServiceModel.Channels.IHttpCookieContainerManager>();
                if ((httpCookieContainerManager != null)) {
                    httpCookieContainerManager.CookieContainer = value;
                }
                else {
                    throw new System.InvalidOperationException("Unable to set the CookieContainer. Please make sure the binding contains an HttpC" +
                            "ookieContainerBindingElement.");
                }
            }
        }
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> InsertCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> EditCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> DeleteCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> OpenCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> CloseCompleted;
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData.BeginInsert(string Name, string Family, string Username, byte[] Password, bool AccessEdit_State1, bool AccessEdit_State2, bool AccessEdit_State3, bool AccessEdit_State4, bool AccessEdit_State5, bool AccessEdit_State6, bool Manager, int State, bool AccessEdit_State7, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginInsert(Name, Family, Username, Password, AccessEdit_State1, AccessEdit_State2, AccessEdit_State3, AccessEdit_State4, AccessEdit_State5, AccessEdit_State6, Manager, State, AccessEdit_State7, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        void IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData.EndInsert(System.IAsyncResult result) {
            base.Channel.EndInsert(result);
        }
        
        private System.IAsyncResult OnBeginInsert(object[] inValues, System.AsyncCallback callback, object asyncState) {
            string Name = ((string)(inValues[0]));
            string Family = ((string)(inValues[1]));
            string Username = ((string)(inValues[2]));
            byte[] Password = ((byte[])(inValues[3]));
            bool AccessEdit_State1 = ((bool)(inValues[4]));
            bool AccessEdit_State2 = ((bool)(inValues[5]));
            bool AccessEdit_State3 = ((bool)(inValues[6]));
            bool AccessEdit_State4 = ((bool)(inValues[7]));
            bool AccessEdit_State5 = ((bool)(inValues[8]));
            bool AccessEdit_State6 = ((bool)(inValues[9]));
            bool Manager = ((bool)(inValues[10]));
            int State = ((int)(inValues[11]));
            bool AccessEdit_State7 = ((bool)(inValues[12]));
            return ((IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData)(this)).BeginInsert(Name, Family, Username, Password, AccessEdit_State1, AccessEdit_State2, AccessEdit_State3, AccessEdit_State4, AccessEdit_State5, AccessEdit_State6, Manager, State, AccessEdit_State7, callback, asyncState);
        }
        
        private object[] OnEndInsert(System.IAsyncResult result) {
            ((IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData)(this)).EndInsert(result);
            return null;
        }
        
        private void OnInsertCompleted(object state) {
            if ((this.InsertCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.InsertCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void InsertAsync(string Name, string Family, string Username, byte[] Password, bool AccessEdit_State1, bool AccessEdit_State2, bool AccessEdit_State3, bool AccessEdit_State4, bool AccessEdit_State5, bool AccessEdit_State6, bool Manager, int State, bool AccessEdit_State7) {
            this.InsertAsync(Name, Family, Username, Password, AccessEdit_State1, AccessEdit_State2, AccessEdit_State3, AccessEdit_State4, AccessEdit_State5, AccessEdit_State6, Manager, State, AccessEdit_State7, null);
        }
        
        public void InsertAsync(string Name, string Family, string Username, byte[] Password, bool AccessEdit_State1, bool AccessEdit_State2, bool AccessEdit_State3, bool AccessEdit_State4, bool AccessEdit_State5, bool AccessEdit_State6, bool Manager, int State, bool AccessEdit_State7, object userState) {
            if ((this.onBeginInsertDelegate == null)) {
                this.onBeginInsertDelegate = new BeginOperationDelegate(this.OnBeginInsert);
            }
            if ((this.onEndInsertDelegate == null)) {
                this.onEndInsertDelegate = new EndOperationDelegate(this.OnEndInsert);
            }
            if ((this.onInsertCompletedDelegate == null)) {
                this.onInsertCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnInsertCompleted);
            }
            base.InvokeAsync(this.onBeginInsertDelegate, new object[] {
                        Name,
                        Family,
                        Username,
                        Password,
                        AccessEdit_State1,
                        AccessEdit_State2,
                        AccessEdit_State3,
                        AccessEdit_State4,
                        AccessEdit_State5,
                        AccessEdit_State6,
                        Manager,
                        State,
                        AccessEdit_State7}, this.onEndInsertDelegate, this.onInsertCompletedDelegate, userState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData.BeginEdit(
                    string Name, 
                    string Family, 
                    string Username, 
                    byte[] Password, 
                    bool AccessEdit_State1, 
                    bool AccessEdit_State2, 
                    bool AccessEdit_State3, 
                    bool AccessEdit_State4, 
                    bool AccessEdit_State5, 
                    bool AccessEdit_State6, 
                    bool Manager, 
                    int State, 
                    long ID, 
                    bool AccessEdit_State7, 
                    System.AsyncCallback callback, 
                    object asyncState) {
            return base.Channel.BeginEdit(Name, Family, Username, Password, AccessEdit_State1, AccessEdit_State2, AccessEdit_State3, AccessEdit_State4, AccessEdit_State5, AccessEdit_State6, Manager, State, ID, AccessEdit_State7, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        void IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData.EndEdit(System.IAsyncResult result) {
            base.Channel.EndEdit(result);
        }
        
        private System.IAsyncResult OnBeginEdit(object[] inValues, System.AsyncCallback callback, object asyncState) {
            string Name = ((string)(inValues[0]));
            string Family = ((string)(inValues[1]));
            string Username = ((string)(inValues[2]));
            byte[] Password = ((byte[])(inValues[3]));
            bool AccessEdit_State1 = ((bool)(inValues[4]));
            bool AccessEdit_State2 = ((bool)(inValues[5]));
            bool AccessEdit_State3 = ((bool)(inValues[6]));
            bool AccessEdit_State4 = ((bool)(inValues[7]));
            bool AccessEdit_State5 = ((bool)(inValues[8]));
            bool AccessEdit_State6 = ((bool)(inValues[9]));
            bool Manager = ((bool)(inValues[10]));
            int State = ((int)(inValues[11]));
            long ID = ((long)(inValues[12]));
            bool AccessEdit_State7 = ((bool)(inValues[13]));
            return ((IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData)(this)).BeginEdit(Name, Family, Username, Password, AccessEdit_State1, AccessEdit_State2, AccessEdit_State3, AccessEdit_State4, AccessEdit_State5, AccessEdit_State6, Manager, State, ID, AccessEdit_State7, callback, asyncState);
        }
        
        private object[] OnEndEdit(System.IAsyncResult result) {
            ((IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData)(this)).EndEdit(result);
            return null;
        }
        
        private void OnEditCompleted(object state) {
            if ((this.EditCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.EditCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void EditAsync(string Name, string Family, string Username, byte[] Password, bool AccessEdit_State1, bool AccessEdit_State2, bool AccessEdit_State3, bool AccessEdit_State4, bool AccessEdit_State5, bool AccessEdit_State6, bool Manager, int State, long ID, bool AccessEdit_State7) {
            this.EditAsync(Name, Family, Username, Password, AccessEdit_State1, AccessEdit_State2, AccessEdit_State3, AccessEdit_State4, AccessEdit_State5, AccessEdit_State6, Manager, State, ID, AccessEdit_State7, null);
        }
        
        public void EditAsync(string Name, string Family, string Username, byte[] Password, bool AccessEdit_State1, bool AccessEdit_State2, bool AccessEdit_State3, bool AccessEdit_State4, bool AccessEdit_State5, bool AccessEdit_State6, bool Manager, int State, long ID, bool AccessEdit_State7, object userState) {
            if ((this.onBeginEditDelegate == null)) {
                this.onBeginEditDelegate = new BeginOperationDelegate(this.OnBeginEdit);
            }
            if ((this.onEndEditDelegate == null)) {
                this.onEndEditDelegate = new EndOperationDelegate(this.OnEndEdit);
            }
            if ((this.onEditCompletedDelegate == null)) {
                this.onEditCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnEditCompleted);
            }
            base.InvokeAsync(this.onBeginEditDelegate, new object[] {
                        Name,
                        Family,
                        Username,
                        Password,
                        AccessEdit_State1,
                        AccessEdit_State2,
                        AccessEdit_State3,
                        AccessEdit_State4,
                        AccessEdit_State5,
                        AccessEdit_State6,
                        Manager,
                        State,
                        ID,
                        AccessEdit_State7}, this.onEndEditDelegate, this.onEditCompletedDelegate, userState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData.BeginDelete(long ID, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginDelete(ID, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        void IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData.EndDelete(System.IAsyncResult result) {
            base.Channel.EndDelete(result);
        }
        
        private System.IAsyncResult OnBeginDelete(object[] inValues, System.AsyncCallback callback, object asyncState) {
            long ID = ((long)(inValues[0]));
            return ((IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData)(this)).BeginDelete(ID, callback, asyncState);
        }
        
        private object[] OnEndDelete(System.IAsyncResult result) {
            ((IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData)(this)).EndDelete(result);
            return null;
        }
        
        private void OnDeleteCompleted(object state) {
            if ((this.DeleteCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.DeleteCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void DeleteAsync(long ID) {
            this.DeleteAsync(ID, null);
        }
        
        public void DeleteAsync(long ID, object userState) {
            if ((this.onBeginDeleteDelegate == null)) {
                this.onBeginDeleteDelegate = new BeginOperationDelegate(this.OnBeginDelete);
            }
            if ((this.onEndDeleteDelegate == null)) {
                this.onEndDeleteDelegate = new EndOperationDelegate(this.OnEndDelete);
            }
            if ((this.onDeleteCompletedDelegate == null)) {
                this.onDeleteCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnDeleteCompleted);
            }
            base.InvokeAsync(this.onBeginDeleteDelegate, new object[] {
                        ID}, this.onEndDeleteDelegate, this.onDeleteCompletedDelegate, userState);
        }
        
        private System.IAsyncResult OnBeginOpen(object[] inValues, System.AsyncCallback callback, object asyncState) {
            return ((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(callback, asyncState);
        }
        
        private object[] OnEndOpen(System.IAsyncResult result) {
            ((System.ServiceModel.ICommunicationObject)(this)).EndOpen(result);
            return null;
        }
        
        private void OnOpenCompleted(object state) {
            if ((this.OpenCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.OpenCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void OpenAsync() {
            this.OpenAsync(null);
        }
        
        public void OpenAsync(object userState) {
            if ((this.onBeginOpenDelegate == null)) {
                this.onBeginOpenDelegate = new BeginOperationDelegate(this.OnBeginOpen);
            }
            if ((this.onEndOpenDelegate == null)) {
                this.onEndOpenDelegate = new EndOperationDelegate(this.OnEndOpen);
            }
            if ((this.onOpenCompletedDelegate == null)) {
                this.onOpenCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnOpenCompleted);
            }
            base.InvokeAsync(this.onBeginOpenDelegate, null, this.onEndOpenDelegate, this.onOpenCompletedDelegate, userState);
        }
        
        private System.IAsyncResult OnBeginClose(object[] inValues, System.AsyncCallback callback, object asyncState) {
            return ((System.ServiceModel.ICommunicationObject)(this)).BeginClose(callback, asyncState);
        }
        
        private object[] OnEndClose(System.IAsyncResult result) {
            ((System.ServiceModel.ICommunicationObject)(this)).EndClose(result);
            return null;
        }
        
        private void OnCloseCompleted(object state) {
            if ((this.CloseCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.CloseCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void CloseAsync() {
            this.CloseAsync(null);
        }
        
        public void CloseAsync(object userState) {
            if ((this.onBeginCloseDelegate == null)) {
                this.onBeginCloseDelegate = new BeginOperationDelegate(this.OnBeginClose);
            }
            if ((this.onEndCloseDelegate == null)) {
                this.onEndCloseDelegate = new EndOperationDelegate(this.OnEndClose);
            }
            if ((this.onCloseCompletedDelegate == null)) {
                this.onCloseCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnCloseCompleted);
            }
            base.InvokeAsync(this.onBeginCloseDelegate, null, this.onEndCloseDelegate, this.onCloseCompletedDelegate, userState);
        }
        
        protected override IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData CreateChannel() {
            return new ServiceMemberEditDataClientChannel(this);
        }
        
        private class ServiceMemberEditDataClientChannel : ChannelBase<IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData>, IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData {
            
            public ServiceMemberEditDataClientChannel(System.ServiceModel.ClientBase<IPLTD.ServiceReferenceMemberEditData.ServiceMemberEditData> client) : 
                    base(client) {
            }
            
            public System.IAsyncResult BeginInsert(string Name, string Family, string Username, byte[] Password, bool AccessEdit_State1, bool AccessEdit_State2, bool AccessEdit_State3, bool AccessEdit_State4, bool AccessEdit_State5, bool AccessEdit_State6, bool Manager, int State, bool AccessEdit_State7, System.AsyncCallback callback, object asyncState) {
                object[] _args = new object[13];
                _args[0] = Name;
                _args[1] = Family;
                _args[2] = Username;
                _args[3] = Password;
                _args[4] = AccessEdit_State1;
                _args[5] = AccessEdit_State2;
                _args[6] = AccessEdit_State3;
                _args[7] = AccessEdit_State4;
                _args[8] = AccessEdit_State5;
                _args[9] = AccessEdit_State6;
                _args[10] = Manager;
                _args[11] = State;
                _args[12] = AccessEdit_State7;
                System.IAsyncResult _result = base.BeginInvoke("Insert", _args, callback, asyncState);
                return _result;
            }
            
            public void EndInsert(System.IAsyncResult result) {
                object[] _args = new object[0];
                base.EndInvoke("Insert", _args, result);
            }
            
            public System.IAsyncResult BeginEdit(
                        string Name, 
                        string Family, 
                        string Username, 
                        byte[] Password, 
                        bool AccessEdit_State1, 
                        bool AccessEdit_State2, 
                        bool AccessEdit_State3, 
                        bool AccessEdit_State4, 
                        bool AccessEdit_State5, 
                        bool AccessEdit_State6, 
                        bool Manager, 
                        int State, 
                        long ID, 
                        bool AccessEdit_State7, 
                        System.AsyncCallback callback, 
                        object asyncState) {
                object[] _args = new object[14];
                _args[0] = Name;
                _args[1] = Family;
                _args[2] = Username;
                _args[3] = Password;
                _args[4] = AccessEdit_State1;
                _args[5] = AccessEdit_State2;
                _args[6] = AccessEdit_State3;
                _args[7] = AccessEdit_State4;
                _args[8] = AccessEdit_State5;
                _args[9] = AccessEdit_State6;
                _args[10] = Manager;
                _args[11] = State;
                _args[12] = ID;
                _args[13] = AccessEdit_State7;
                System.IAsyncResult _result = base.BeginInvoke("Edit", _args, callback, asyncState);
                return _result;
            }
            
            public void EndEdit(System.IAsyncResult result) {
                object[] _args = new object[0];
                base.EndInvoke("Edit", _args, result);
            }
            
            public System.IAsyncResult BeginDelete(long ID, System.AsyncCallback callback, object asyncState) {
                object[] _args = new object[1];
                _args[0] = ID;
                System.IAsyncResult _result = base.BeginInvoke("Delete", _args, callback, asyncState);
                return _result;
            }
            
            public void EndDelete(System.IAsyncResult result) {
                object[] _args = new object[0];
                base.EndInvoke("Delete", _args, result);
            }
        }
    }
}
