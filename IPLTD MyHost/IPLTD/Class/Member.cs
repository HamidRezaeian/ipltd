﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IPLTD.Class
{

    public class Member
    {

        public long ID {get;set;}
        public string Name { get; set; }
        public string Family { get; set; }
        public string UserName { get; set; }
        public byte[] Password { get; set; }
        public bool Manager { get; set; }
        public bool AccessEdit_State1 { get; set; }
        public bool AccessEdit_State2 { get; set; }
        public bool AccessEdit_State3 { get; set; }
        public bool AccessEdit_State4 { get; set; }
        public bool AccessEdit_State5 { get; set; }
        public bool AccessEdit_State6 { get; set; }
        public Member()
        {
        }
    }
}