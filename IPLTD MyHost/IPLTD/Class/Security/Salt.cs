﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace IPLTD.Class.Security
{
    public class SaltedHash
    {
        HashAlgorithm HashProvider;
        int SalthLength;
        public SaltedHash(HashAlgorithm HashAlgorithm, int theSaltLength)
        {
          
            HashProvider = HashAlgorithm;
            SalthLength = theSaltLength;
        }

        public SaltedHash() : this(new SHA256Managed(), 4)
        {
        }
        private byte[] ComputeHash(byte[] Data, byte[] Salt)
        {
            byte[] DataAndSalt = new byte[Data.Length + SalthLength];
            Array.Copy(Data, DataAndSalt, Data.Length);
            Array.Copy(Salt, 0, DataAndSalt, Data.Length, SalthLength);
            return HashProvider.ComputeHash(DataAndSalt);
        }

        public void GetHashAndSalt(byte[] Data, out byte[] Hash, out byte[] Salt)
        {
            Salt = new byte[SalthLength];
            RNGCryptoServiceProvider random = new RNGCryptoServiceProvider();
            random.GetBytes(Salt);

            Hash = ComputeHash(Data, Salt);
        }
        public void GetHashAndSaltString(string Data, out string Hash, out string Salt)
        {
            byte[] HashOut;
            byte[] SaltOut;
            GetHashAndSalt(Encoding.UTF8.GetBytes(Data), out HashOut, out SaltOut);
            Hash = Convert.ToBase64String(HashOut);
            Salt = Convert.ToBase64String(SaltOut);
        }
        public bool VerifyHash(byte[] Data, byte[] Hash, byte[] Salt)
        {
            byte[] NewHash = ComputeHash(Data, Salt);
            if (NewHash.Length != Hash.Length) return false;
            for (int Lp = 0; Lp < Hash.Length; Lp++)
                if (!Hash[Lp].Equals(NewHash[Lp]))
                    return false;
            return true;
        }
        public bool VerifyHashString(string Data, string Hash, string Salt)
        {
          
            byte[] HashToVerify = Convert.FromBase64String(Hash);
            byte[] SaltToVerify = Convert.FromBase64String(Salt);
            byte[] DataToVerify = Encoding.UTF8.GetBytes(Data);
           
            return VerifyHash(DataToVerify, HashToVerify, SaltToVerify);
        }



        public byte[] GetSaltCompute(string Data)
        {


            byte[] Salt1;
            byte[] Data1;
            Data1 = Encoding.UTF8.GetBytes(Data);
            Salt1 = new byte[SalthLength];
            RNGCryptoServiceProvider random = new RNGCryptoServiceProvider();
            random.GetBytes(Salt1);
//---------------------------------------------------------
            byte[] DataAndSalt = new byte[Data1.Length + SalthLength];
            Array.Copy(Data1, DataAndSalt, Data1.Length);
            Array.Copy(Salt1, 0, DataAndSalt, Data1.Length, SalthLength);
            return HashProvider.ComputeHash(DataAndSalt);
        }





        public  Byte[] hash(string str)
        {

            SHA256Managed crypt = new SHA256Managed();
            Byte[] bytes;
            bytes = Encoding.UTF8.GetBytes(str);
            return crypt.ComputeHash(bytes);
        }

    }
}

 