﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace IPLTD.Class
{
    public class IPL
    {

        public long __ID { get; set; }
        public string __Telephone { get; set; }
        public int __State { get; set; }
        public string __Kafoo { get; set; }
        public string __Post_Master { get; set; }
        public string __Connection { get; set; }
        public string __Post1 { get; set; }
        public string __Post2 { get; set; }
        public string __Post3 { get; set; }
        public string __Post4 { get; set; }
        public string __Post5 { get; set; }
        public string __User_ { get; set; }
        public string __Position { get; set; }
        

        public IPL()
        {
        }
    }
}
