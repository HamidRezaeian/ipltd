﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;

namespace IPLTD
{
    public partial class CustomMessage : ChildWindow
    {
        public enum MessageType { Info, Error, Confirm, TextInput, ComboInput };
        public string Input { get; set; }
        private const string ICONS_PATH = "/IPLTD;component/Images/";

        public CustomMessage(string message, MessageType type = MessageType.Info, String[] inputOptions = null)
        {
            InitializeComponent();
            switch (type)
            {
                case MessageType.Info:
                break;
                case MessageType.TextInput:
                case MessageType.ComboInput:
                    this.TextBlock.VerticalAlignment = VerticalAlignment.Top;
                    Thickness newBorderMargin = this.TextBlockBorder.Margin;
                    newBorderMargin.Top += 5;
                    this.TextBlockBorder.Margin = newBorderMargin;
                    if (type == MessageType.ComboInput)
                    {
                        this.InputComboBox.ItemsSource = inputOptions;
                        this.InputComboBox.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        this.InputTextBox.Visibility = Visibility.Visible;
                    }
                    break;

                case MessageType.Error:
                    setMessageIcon("button_cancel-256.png");
                    this.MessageBorder.BorderBrush = new SolidColorBrush(Colors.Red);
                    break;
                case MessageType.Confirm:
                    setMessageIcon("Bullet-question-256.png");
                    this.MessageBorder.BorderBrush = new SolidColorBrush(Colors.Orange);
                    this.OKButton.Content = "Yes";
                    this.CancelButton.Content = "No";
                    this.CancelButton.Visibility = Visibility.Visible;

                    break;
            }
            this.TextBlock.Text = message;
        }
        private void setMessageIcon(string imagePath)
        {
            MessageIcon.Source = new BitmapImage(new Uri(ICONS_PATH + imagePath, UriKind.RelativeOrAbsolute));
        }
        #region Button Hanlders
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.InputTextBox.Visibility == Visibility.Visible
                && this.InputTextBox.Text.Length > 0)
                Input = InputTextBox.Text;
            else if (this.InputComboBox.Visibility == Visibility.Visible
                    && this.InputComboBox.SelectedItem != null)
                Input = (String)InputComboBox.SelectedValue;
            else
                Input = null;

            this.DialogResult = true;
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        #endregion
        #region Keyboad Event Handlers
        private void keyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ButtonAutomationPeer buttonAutoPeer = new ButtonAutomationPeer(OKButton);
                IInvokeProvider invokeProvider = buttonAutoPeer.GetPattern(PatternInterface.Invoke) as IInvokeProvider;
                invokeProvider.Invoke();
            }
        }
        #endregion
    }
}
