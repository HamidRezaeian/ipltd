CREATE ROLE [aspnet_Membership_BasicAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Membership_ReportingAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Membership_FullAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Personalization_BasicAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Personalization_ReportingAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Personalization_FullAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Profile_BasicAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Profile_ReportingAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Profile_FullAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Roles_BasicAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Roles_ReportingAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_Roles_FullAccess] AUTHORIZATION [dbo]
GO
CREATE ROLE [aspnet_WebEvent_FullAccess] AUTHORIZATION [dbo]
GO
CREATE SCHEMA [aspnet_WebEvent_FullAccess] AUTHORIZATION [aspnet_WebEvent_FullAccess]
GO
CREATE SCHEMA [aspnet_Roles_ReportingAccess] AUTHORIZATION [aspnet_Roles_ReportingAccess]
GO
CREATE SCHEMA [aspnet_Roles_FullAccess] AUTHORIZATION [aspnet_Roles_FullAccess]
GO
CREATE SCHEMA [aspnet_Roles_BasicAccess] AUTHORIZATION [aspnet_Roles_BasicAccess]
GO
CREATE SCHEMA [aspnet_Profile_ReportingAccess] AUTHORIZATION [aspnet_Profile_ReportingAccess]
GO
CREATE SCHEMA [aspnet_Profile_FullAccess] AUTHORIZATION [aspnet_Profile_FullAccess]
GO
CREATE SCHEMA [aspnet_Profile_BasicAccess] AUTHORIZATION [aspnet_Profile_BasicAccess]
GO
CREATE SCHEMA [aspnet_Personalization_ReportingAccess] AUTHORIZATION [aspnet_Personalization_ReportingAccess]
GO
CREATE SCHEMA [aspnet_Personalization_FullAccess] AUTHORIZATION [aspnet_Personalization_FullAccess]
GO
CREATE SCHEMA [aspnet_Personalization_BasicAccess] AUTHORIZATION [aspnet_Personalization_BasicAccess]
GO
CREATE SCHEMA [aspnet_Membership_ReportingAccess] AUTHORIZATION [aspnet_Membership_ReportingAccess]
GO
CREATE SCHEMA [aspnet_Membership_FullAccess] AUTHORIZATION [aspnet_Membership_FullAccess]
GO
CREATE SCHEMA [aspnet_Membership_BasicAccess] AUTHORIZATION [aspnet_Membership_BasicAccess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mailbox](
	[Subject] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[From_m] [nchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Date] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Size] [nchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[To_m] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Read_s] [bit] NULL,
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Header] [nchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Mailbox] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IPL](
	[State] [int] NOT NULL,
	[Post1] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Post2] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Post3] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Post4] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Post5] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[User_] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Kafoo] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Telephone] [nchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Post_Master] [nchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Connection] [nchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Position] [nchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_IPL] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

GO
SET IDENTITY_INSERT [dbo].[IPL] ON 

GO
INSERT [dbo].[IPL] ([State], [Post1], [Post2], [Post3], [Post4], [Post5], [User_], [Kafoo], [ID], [Telephone], [Post_Master], [Connection], [Position]) VALUES (2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 280, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[IPL] ([State], [Post1], [Post2], [Post3], [Post4], [Post5], [User_], [Kafoo], [ID], [Telephone], [Post_Master], [Connection], [Position]) VALUES (2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 281, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[IPL] OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.Restore1
@DataPath Nvarchar(1024),
@DB nvarchar(1024)
AS
ALTER DATABASE  [@DB]  SET SINGLE_USER with ROLLBACK IMMEDIATE
RESTORE DATABASE [@DB] FROM DISK=@DataPath with REPLACE
ALTER DATABASE [@DB] SET MULTI_USER

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.Backup1
@DataPath Nvarchar(1024),
@DB nvarchar(1024)
AS
BACKUP DATABASE @DB TO DISK=@DataPath with NOFORMAT,NOINIT,NAME=N'database.MDF-Full Database Backup',SKIP,NOREWIND,NOUNLOAD,STATS=10



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Members](
	[Name] [nchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Family] [nchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Username] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Password] [varbinary](50) NOT NULL,
	[AccessEdit_State1] [bit] NULL,
	[AccessEdit_State2] [bit] NULL,
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AccessEdit_State3] [bit] NULL,
	[AccessEdit_State4] [bit] NULL,
	[AccessEdit_State5] [bit] NULL,
	[AccessEdit_State6] [bit] NULL,
	[Manager] [bit] NULL,
	[State] [int] NULL,
	[AccessEdit_State7] [bit] NULL,
 CONSTRAINT [PK_Members] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

GO
SET IDENTITY_INSERT [dbo].[Members] ON 

GO
INSERT [dbo].[Members] ([Name], [Family], [Username], [Password], [AccessEdit_State1], [AccessEdit_State2], [ID], [AccessEdit_State3], [AccessEdit_State4], [AccessEdit_State5], [AccessEdit_State6], [Manager], [State], [AccessEdit_State7]) VALUES (N'حمید           ', N'رضاییان             ', N'hamidhws', 0xE3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855, 1, 1, 20, 1, 1, 1, 1, 1, 2, 0)
GO
SET IDENTITY_INSERT [dbo].[Members] OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.RestoreDatabase
@DataPath Nvarchar(1024),
@DB nvarchar(1024)


AS
RESTORE DATABASE @DB FROM DISK=@DataPath with REPLACE
 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.RestoreDatabase1
@DataPath Nvarchar(1024),
@DB nvarchar(1024)


AS

RESTORE DATABASE @DB FROM DISK=@DataPath with REPLACE
 


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.StoredProcedure1
	
 @disk nvarchar(1000),
@dbname nvarchar(255)
 AS
 
exec('use master '+
'alter database '+@dbname+' set single_user with rollback immediate '+
'restore database '+@dbname+'   from disk =N'''+@disk+''' '+
'alter database '+@dbname+' set  multi_user')

GO
ALTER TABLE [dbo].[Mailbox] ADD  CONSTRAINT [DF_Mailbox_Read_s]  DEFAULT ((0)) FOR [Read_s]
GO
ALTER TABLE [dbo].[Members] ADD  CONSTRAINT [DF_Members_AccessEdit_State1]  DEFAULT ((0)) FOR [AccessEdit_State1]
GO
ALTER TABLE [dbo].[Members] ADD  CONSTRAINT [DF_Members_AccessEdit_State2]  DEFAULT ((0)) FOR [AccessEdit_State2]
GO
ALTER TABLE [dbo].[Members] ADD  CONSTRAINT [DF_Members_AccessEdit_State3]  DEFAULT ((0)) FOR [AccessEdit_State3]
GO
ALTER TABLE [dbo].[Members] ADD  CONSTRAINT [DF_Members_AccessEdit_State4]  DEFAULT ((0)) FOR [AccessEdit_State4]
GO
ALTER TABLE [dbo].[Members] ADD  CONSTRAINT [DF_Members_AccessEdit_State5]  DEFAULT ((0)) FOR [AccessEdit_State5]
GO
ALTER TABLE [dbo].[Members] ADD  CONSTRAINT [DF_Members_AccessEdit_State6]  DEFAULT ((0)) FOR [AccessEdit_State6]
GO
ALTER TABLE [dbo].[Members] ADD  CONSTRAINT [DF_Members_Manager_1]  DEFAULT ((0)) FOR [Manager]
GO
ALTER TABLE [dbo].[Members] ADD  CONSTRAINT [DF_Members_AccessEdit_State7]  DEFAULT ((0)) FOR [AccessEdit_State7]
GO
