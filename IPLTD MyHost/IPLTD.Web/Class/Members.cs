﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace IPLTD.Web.Class
{

    public class Members
    {

        public long _ID;
        public string _Name;
        public string _Family;
        public string _UserName;
        public byte[] _Password;
        public bool _Manager;
        public bool _AccessEdit_State1;
        public bool _AccessEdit_State2;
        public bool _AccessEdit_State3;
        public bool _AccessEdit_State4;
        public bool _AccessEdit_State5;
        public bool _AccessEdit_State6;
        public bool _AccessEdit_State7;
        public int ___State;
        public Members()
        {
        }
    }
}