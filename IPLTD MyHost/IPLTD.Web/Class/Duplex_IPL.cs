﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace IPLTD.Web.Class
{
    [ServiceContract]
    interface Duplex_IPL
    {
        [OperationContract(IsOneWay = true)]
        void OnMessage(string Action, int Index, string Telephone, int State, string Post_Master, string Connection, string Post1, string Post2, string Post3, string Post4, string Post5, string User_, string Kafoo,string Position, Int64 ID);
    }
}
