﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLTD.Web.Class
{
    public class MailboxClass
    {
        public long _ID;
        public string _Subject;
        public string _From;
        public string _Date;
        public string _Size;
        public string _To;
        public bool _Read;
        public string _Header;

        public MailboxClass()
        {
        }
    }
}