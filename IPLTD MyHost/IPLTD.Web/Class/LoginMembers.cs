﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLTD.Web.Class
{
    public class LoginMembers
    {
 
        public string Name;
        public string Family;
        public string UserName;
        public bool Manager;
        public bool AccessEdit_State1;
        public bool AccessEdit_State2;
        public bool AccessEdit_State3;
        public bool AccessEdit_State4;
        public bool AccessEdit_State5;
        public bool AccessEdit_State6;
        public bool AccessEdit_State7;
        public int State;
        public bool Access=false;
        public LoginMembers()
        {
        }
    }
}