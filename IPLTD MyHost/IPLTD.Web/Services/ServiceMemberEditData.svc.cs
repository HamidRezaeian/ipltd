﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace IPLTD.Web.Services
{
    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ServiceMemberEditData
    {
        [OperationContract]
        public void Insert(string Name, string Family, string Username, byte[] Password, bool AccessEdit_State1, bool AccessEdit_State2, bool AccessEdit_State3, bool AccessEdit_State4, bool AccessEdit_State5, bool AccessEdit_State6, bool Manager, int State, bool AccessEdit_State7)
        {
        
            DataSetTableAdapters.MembersTableAdapter adapter = new DataSetTableAdapters.MembersTableAdapter();
            adapter.Insert(Name,Family,Username,Password, AccessEdit_State1,AccessEdit_State2, AccessEdit_State3,AccessEdit_State4,AccessEdit_State5,AccessEdit_State6,Manager,State,AccessEdit_State7);
        }

        [OperationContract]
        public void Edit(string Name, string Family, string Username, byte[] Password, bool AccessEdit_State1, bool AccessEdit_State2, bool AccessEdit_State3, bool AccessEdit_State4, bool AccessEdit_State5, bool AccessEdit_State6, bool Manager, int State, Int64 ID, bool AccessEdit_State7)
        {
            DataSetTableAdapters.MembersTableAdapter adapter = new DataSetTableAdapters.MembersTableAdapter();
            adapter.Update(Name, Family, Username, Password, AccessEdit_State1, AccessEdit_State2, AccessEdit_State3, AccessEdit_State4, AccessEdit_State5, AccessEdit_State6, Manager, State, AccessEdit_State7, ID);
        }

        [OperationContract]
        public void Delete(Int64 ID)
        {
            DataSetTableAdapters.MembersTableAdapter adapter = new DataSetTableAdapters.MembersTableAdapter();
            adapter.Delete(ID);
        }


    }
}
