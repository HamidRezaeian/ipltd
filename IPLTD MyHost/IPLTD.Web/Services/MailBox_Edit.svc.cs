﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace IPLTD.Web.Services
{
    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MailBox_Edit
    {
        [OperationContract]
        public void Insert(string Subject, string From_m, string Date, string Size, string To_m, bool Read_s, string Header)
        {
            string StrTime;
            StrTime = Convert.ToString(DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day);
            Date = StrTime;
            DataSetTableAdapters.MailboxTableAdapter adapter = new DataSetTableAdapters.MailboxTableAdapter();
            adapter.Insert(Subject, From_m, Date, Size, To_m, Read_s, Header);
        }
        [OperationContract]
        public void Update(string Subject, string From_m, string Date, string Size, string To_m, bool Read_s, string Header,Int64 ID)
        {
            DataSetTableAdapters.MailboxTableAdapter adapter = new DataSetTableAdapters.MailboxTableAdapter();
            adapter.Update(Subject, From_m, Date, Size, To_m,Read_s, Header,ID);
        }
        [OperationContract]
        public void Delete(Int64 ID)
        {
            DataSetTableAdapters.MailboxTableAdapter adapter = new DataSetTableAdapters.MailboxTableAdapter();
            adapter.Delete(ID);
        }



    }
}
