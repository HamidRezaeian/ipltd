﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using IPLTD.Web.Class;

namespace IPLTD.Web.Services
{
    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ServiceIPLEditData
    {
        long FindID;
        [OperationContract]
        public long Insert(string Telephone, int State, string Post_Master, string Connection, string Post1, string Post2, string Post3, string Post4, string Post5, string User_, string Kafoo,string Position)
        {
            DataSetTableAdapters.IPLTableAdapter adapter = new DataSetTableAdapters.IPLTableAdapter();
            adapter.Insert(State,Post1,Post2,Post3,Post4,Post5,User_,Kafoo,Telephone,Post_Master,Connection,Position);
            DataSet.IPLDataTable table = adapter.GetData();
            foreach (var item in table)
            {
                FindID = item.ID;
                
            }
     

            return FindID;
            
        }

        [OperationContract]
        public long Edit(string Telephone, int State, string Post_Master, string Connection, string Post1, string Post2, string Post3, string Post4, string Post5, string User_, string Kafoo,string Position,Int64 ID)
        {

            DataSetTableAdapters.IPLTableAdapter adapter = new DataSetTableAdapters.IPLTableAdapter();
            adapter.Update(State, Post1, Post2, Post3, Post4, Post5, User_, Kafoo, Telephone, Post_Master, Connection,Position, ID);
     
            DataSet.IPLDataTable table = adapter.GetData();
            foreach (var item in table)
            {
                FindID = item.ID;

            }

            return FindID;
        }


        [OperationContract]
        public void Delete(Int64 ID)
        {
            DataSetTableAdapters.IPLTableAdapter adapter = new DataSetTableAdapters.IPLTableAdapter();
            adapter.Delete(ID);
        }
    }
}
