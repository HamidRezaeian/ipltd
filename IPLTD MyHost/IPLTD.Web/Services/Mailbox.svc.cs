﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using IPLTD.Web.Class;
using System.Collections.Generic;

namespace IPLTD.Web.Services
{
    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Mailbox
    {
        [OperationContract]
        public List<MailboxClass> GetAllInformation(string Set_GetID)
        {
            List<MailboxClass> _result = new List<MailboxClass>();

            DataSetTableAdapters.MailboxTableAdapter adapter = new DataSetTableAdapters.MailboxTableAdapter();
            DataSet.MailboxDataTable table = adapter.GetData();
            foreach (var item in table)
            {
                MailboxClass _Mail = new MailboxClass();

                
                _Mail._Subject = item.Subject;
                _Mail._ID = item.ID;
                _Mail._Size = item.Size;
                _Mail._Read = item.Read_s;
                _Mail._From = item.From_m;
                _Mail._Date = item.Date;
                _Mail._To = item.To_m;
                _Mail._Header = item.Header;

                if (Set_GetID==item.To_m)
                _result.Add(_Mail);
            }

            return _result;
        }
    }
}