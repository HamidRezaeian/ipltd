﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using Microsoft.SqlServer.Server;
using Microsoft.SqlServer.Management.Smo;
namespace IPLTD.Web.Services
{
    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ServiceDatabaseManagment
    {
    
        
        [OperationContract]
        public void BackUp()
        {
         //   SqlConnection connection = new SqlConnection();
         //   connection.ConnectionString = @"Data Source=.\SQLEXPRESS;AttachDbFilename='C:\Users\hamid\Documents\Visual Studio 2010\Projects\IPLTD\IPLTD.Web\App_Data\IPLTD_Data.mdf';Integrated Security=True;Pooling=False;User Instance=True";
         //   SqlCommand comm = new SqlCommand();
         //   comm.CommandText = "Backup1";
         //   comm.CommandType = CommandType.StoredProcedure;
         //   comm.Connection = connection;
         //   SqlParameter param = comm.CreateParameter();
         //   SqlParameter param1 = new SqlParameter();
        //    connection.Open();
       //     param1.Value = @"C:\Users\hamid\Documents\Visual Studio 2010\Projects\IPLTD\IPLTD.Web\App_Data\IPLTD_Data.mdf";
      //      //    param1.Value = AppDomain.CurrentDomain.BaseDirectory + @"DB\database.mdf";
      //      param.Value = @"C:\Drive\HWS.bak";
     //       param1.ParameterName = "@DB";
     //       param.ParameterName = "@DataPath";
     //       param.DbType = DbType.String;
     //       comm.Parameters.Add(param);
     //       comm.Parameters.Add(param1);
     //       comm.ExecuteNonQuery();
    //        connection.Close();

                if (Sqlsrv != null)
                {
                        Backup budb = new Backup();
                        budb.Action = BackupActionType.Database;
                        budb.Database = "IPLTD(Data)".Trim();
                        BackupDeviceItem budevice = new BackupDeviceItem(AppDomain.CurrentDomain.BaseDirectory + @"Backupdb\HWS.bak", DeviceType.File);
                        budb.Initialize = true;
                        budb.Checksum = true;
                        budb.ContinueAfterError = true;
                        budb.Incremental = false;
                        budb.LogTruncation = BackupTruncateLogType.Truncate;
                        budb.Devices.Add(budevice);
                        budb.PercentComplete += new PercentCompleteEventHandler(backup_PercentComplete);
                        budb.PercentCompleteNotification = 1;
                        /*backup.Complete +=new Microsoft.SqlServer.Management.Common.ServerMessageEventHandler(backup_Complete);*/
                        budb.SqlBackup(Sqlsrv);
                        //        File.Copy(AppDomain.CurrentDomain.BaseDirectory + @"Backupdb\HWS.bak", AppDomain.CurrentDomain.BaseDirectory + "HWS.bak",true);
                   
                }






        }
        public event PercentCompleteEventHandler PercentComplete;
        private void backup_PercentComplete(object sender, PercentCompleteEventArgs e)
        {
            if (this.PercentComplete != null)
            {
                this.PercentComplete(sender, e);
               // progressBar1.Value = e.Percent;
            }
        }
        private void AppRefresh()
        {
        //    cmbServers.Items.Clear();
          //  cmbDataBases.Items.Clear();
        }
         Server Sqlsrv=new Server(@".");
         [OperationContract]
         public void Restore()
         {


             //Microsoft.SqlServer.Management.Smo.DatabaseFile;

             SqlConnection.ClearAllPools();
             if (Sqlsrv != null)
             {

                 Restore sqlRestore = new Restore();
                 //sqlRestore.PercentComplete += new PercentCompleteEventHandler(action_PercentComplete);
                 sqlRestore.PercentCompleteNotification = 1;
                 BackupDeviceItem deviceItem = new BackupDeviceItem(AppDomain.CurrentDomain.BaseDirectory + @"Backupdb\HWS.bak", DeviceType.File);
                 sqlRestore.Devices.Add(deviceItem);
                 sqlRestore.Action = RestoreActionType.Database;
                 sqlRestore.Database = "IPLTD(Data)".Trim();
                 RelocateFile rf;
                 DataTable dt = sqlRestore.ReadFileList(Sqlsrv);
                 foreach (DataRow r in dt.Rows)
                 {
                     if (r.ItemArray[2].ToString() == "D")
                     {
                         rf = new RelocateFile(r.ItemArray[0].ToString(), Sqlsrv.Databases["IPLTD(Data)".Trim()].FileGroups[0].Files[0].FileName);
                     }
                     else if (r.ItemArray[2].ToString() == "L")
                     {
                         rf = new RelocateFile(r.ItemArray[0].ToString(), Sqlsrv.Databases["IPLTD(Data)".Trim()].LogFiles[0].FileName);
                     }
                     else
                     {
                         throw new Exception("File type invalid!!" + "\n" + "نوع فایل ناشناخته است");
                     }
                     sqlRestore.RelocateFiles.Add(rf);
                 }
                 sqlRestore.ReplaceDatabase = true;
                 //sqlRestore.NoRecovery=true;
                 Sqlsrv.Databases["IPLTD(Data)".Trim()].SetOnline();
                 sqlRestore.SqlRestore(Sqlsrv);

             }
         }
    
    }
}
