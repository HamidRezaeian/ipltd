﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using IPLTD.Web.Class;
using System.Collections.Generic;
namespace IPLTD.Web.Services
{
    [ServiceContract(Namespace = "", CallbackContract = typeof(Duplex_IPL))]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]

    public class ServiceDuplex_IPL
    {
        private SynchronizedCollection<Duplex_IPL> m_Clients = new SynchronizedCollection<Duplex_IPL>();
        //--------------------------------------------------------------
        [OperationContract(IsOneWay = true)]
        public void AddClient()
        {
            var newClient = OperationContext.Current.GetCallbackChannel<Duplex_IPL>();
            m_Clients.Add(newClient);

        }

        [OperationContract(IsOneWay = true)]
        public void RemoveClient()
        {
            var Client = OperationContext.Current.GetCallbackChannel<Duplex_IPL>();
            m_Clients.Remove(Client);
        }

        [OperationContract(IsOneWay = true)]
        public void SendMessage(string Action, int Index, string Telephone, int State, string Post_Master, string Connection, string Post1, string Post2, string Post3, string Post4, string Post5, string User_, string Kafoo, string Position, Int64 ID)
        {
            foreach (var client in m_Clients)
            {
                client.OnMessage(Action, Index, Telephone, State, Post_Master, Connection, Post1, Post2, Post3, Post4, Post5, User_, Kafoo, Position, ID);
            }

        }

    }
}