﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using IPLTD.Web.Class;

namespace IPLTD.Web.Services
{
    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ServiceMembers
    {
        [OperationContract]
        public List<Members> GetAllMember()
        {
            
                List<Members> _result = new List<Members>();

                DataSetTableAdapters.MembersTableAdapter adapter = new DataSetTableAdapters.MembersTableAdapter();

                DataSet.MembersDataTable table = adapter.GetData();

                foreach (var item in table)
                {
                    Members _Member = new Members();

                    _Member._ID = item.ID;

                    _Member._Name = item.Name;
                    _Member._Family = item.Family;
                    _Member._UserName = item.Username;
                    _Member._Password = item.Password;
                    _Member._Manager = item.Manager;
                    _Member._AccessEdit_State1 = item.AccessEdit_State1;
                    _Member._AccessEdit_State2 = item.AccessEdit_State2;
                    _Member._AccessEdit_State3 = item.AccessEdit_State3;
                    _Member._AccessEdit_State4 = item.AccessEdit_State4;
                    _Member._AccessEdit_State5 = item.AccessEdit_State5;
                    _Member._AccessEdit_State6 = item.AccessEdit_State6;
                    _Member._AccessEdit_State7 = item.AccessEdit_State7;
                    _Member.___State = item.State;
                    _result.Add(_Member);
                }
            
            return _result;
            
        }
    }
}
