﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using IPLTD.Web.Class;


namespace IPLTD.Web.Services
{
    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ServiceIPLT
    {
        [OperationContract]
        public List<_IPL> GetAllInformation()
        {
            List<_IPL> _result = new List<_IPL>();

            DataSetTableAdapters.IPLTableAdapter adapter = new DataSetTableAdapters.IPLTableAdapter();
            DataSet.IPLDataTable table = adapter.GetData();
            foreach (var item in table)
            {
                _IPL _IPL_ = new _IPL();

               
                _IPL_._Connection = item.Connection;
                _IPL_._ID = item.ID;
                _IPL_._Kafoo = item.Kafoo;
                _IPL_._Post_Master = item.Post_Master;
                _IPL_._Post1 = item.Post1;
                _IPL_._Post2 = item.Post2;
                _IPL_._Post3 = item.Post3;
                _IPL_._Post4 = item.Post4;
                _IPL_._Post5 = item.Post5;
                _IPL_._State = item.State;
                _IPL_._Telephone = item.Telephone;
                _IPL_._User_ = item.User_;
                _IPL_._Position = item.Position;

                _result.Add(_IPL_);
            }

            return _result;
        }

    }
}