﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using IPLTD.Web.Class;

namespace IPLTD.Web.Services
{

    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ServiceLogin
    {

        [OperationContract]
        public LoginMembers Login(string Username, byte[] Password)
        {
            LoginMembers _LoginMembers = new LoginMembers();
            DataSetTableAdapters.MembersTableAdapter adapter = new DataSetTableAdapters.MembersTableAdapter();
            DataSet.MembersDataTable table = adapter.GetData();
            foreach (var item in table)
            {

                if (item.Username == Username)
                {
                    if (comparetwobytes(Password, item.Password) == true)
                    {
                        _LoginMembers.Access = true;
                        _LoginMembers.State = item.State;
                        _LoginMembers.AccessEdit_State1 = item.AccessEdit_State1;
                        _LoginMembers.AccessEdit_State2 = item.AccessEdit_State2;
                        _LoginMembers.AccessEdit_State3 = item.AccessEdit_State3;
                        _LoginMembers.AccessEdit_State4 = item.AccessEdit_State4;
                        _LoginMembers.AccessEdit_State5 = item.AccessEdit_State5;
                        _LoginMembers.AccessEdit_State6 = item.AccessEdit_State6;
                        _LoginMembers.AccessEdit_State7 = item.AccessEdit_State7;
                        _LoginMembers.Family = item.Family;
                        _LoginMembers.Manager = item.Manager;
                        _LoginMembers.Name = item.Name;
                        _LoginMembers.UserName = item.Username;
                        break;
                    }
                }
            }
            return _LoginMembers;
        }



        private Boolean comparetwobytes(Byte[] tmphash, Byte[] tmpnewhash)
        {
            bool bEqual = false;
            if (tmpnewhash.Length == tmphash.Length)
            {
                int i = 0;
                while ((i < tmpnewhash.Length) && (tmpnewhash[i] == tmphash[i]))
                {
                    i += 1;
                }
                if (i == tmpnewhash.Length)
                {
                    bEqual = true;
                }
            }
            return bEqual;
        }


    }
}
